#! /usr/bin/env python3.4
# Choose the good grid and initialised the simulation with a run
# without turbines

import math
import fileops
import os
import subprocess
import shutil

def init_cycle_without_turb_v2(mesh, num_procs, phi, amplitude, option):

  dir_run = option['dir_run']
  folder = fileops.foldername(mesh, num_procs, phi, amplitude)
  dir_all = dir_run + '/' + option['dir_simu'] + '/' + folder
  os.makedirs(dir_all)

  # Rebuilds grid based on 'mesh' from files in /ICEM folder
  if option['regrid'] == 1:
    os.chdir(dir_run + '/ICEM')
    subprocess.call([dir_run + '/scripts/msh2neu/msh2neu', mesh + '.msh', mesh + '.neu'])

    fileops.replacelines(dir_run + '/scripts/neu2suntans_perso2.m', ['mesh_file='], ['mesh_file= \'' + mesh + '.neu\';\n'])
    subprocess.call([dir_run + '/scripts/matlab_neu2suntans', dir_run + '/scripts/neu2suntans_perso2.m'])
    shutil.move('points.dat', dir_run + '/rundata/points.dat')
    shutil.move('cells.dat', dir_run + '/rundata/cells.dat')
    shutil.move('edges.dat', dir_run + '/rundata/edges.dat')
  
  # Modification of Suntans.dat:
  fileops.replacelines(dir_run + '/rundata/suntans.dat', 
    ['amp ', 'omega ', 'phase_diff ', 'dt ', 'nsteps ', 'ntconserve ', 'n_rows '],
    ['amp \t' + str(amplitude) + '\t# amplitude \n',
     'omega \t' +  str(option['omega'])+'\t# frequency \n',
     'phase_diff \t' + str(phi * math.pi / 180) + '\t# phase difference between tidal signals \n',
     'dt \t' + str(option['pdt_init']) + '\t# frequency \n',
     'nsteps \t' + str(option['nsteps_init']) + '\t # Number of steps \n',
     'ntconserve \t' + str(option['n_conserve']) + '\t# How often to output conserved data \n',
     'n_rows \t 0 # number of rows of turbines \n'])

  # Run the initial suntans simulation
  fileops.runsuntans(option, dir_all + '/' + option['dir_init'], num_procs, 0)

  return

