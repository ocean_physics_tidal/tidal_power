import shutil
import subprocess
import os
import numpy as np

# Takes a filename and two string arrays
# Opens 'filename' and replaces lines that contain a string in 'find'
# To the matching string in 'replace'
# Might be clearer using a map
def replacelines(filename, find, replace):

  shutil.copyfile(filename, filename + '~')
  file_ref = open(filename + '~', 'r')
  file_modif = open(filename, 'w')
  lines = file_ref.readlines()
  
  for line in lines:
    line_new = line
    for i in range(len(find)):
      if find[i] in line:
        line_new = replace[i]
    file_modif.write(line_new)

  file_ref.close()
  file_modif.close()

# For running SUNTANS
# mode = 0 for initial run, mode = 1 for subsequent runs 
def runsuntans(option, sim_dir, procs, mode):

  verbose = ''
  if option['Suntans_info'] == 1:
    verbose = ' -vv'

  if mode == 0:
    suntans_args = '"-s' + verbose + '"'
  else:
    suntans_args = '"-s -r' + verbose + '"'

  dir_run = option['dir_run']
  os.chdir(dir_run)
  process = subprocess.Popen([dir_run + '/Suntans_run.sh', option['dir_sources'], 
    sim_dir, str(procs), option['runwith'], suntans_args])
  process.communicate()  # now wait

#################SAVE PARAMETERS IN A FILE#########
def saveoptions(option, filename):
  File = open(filename, 'w')
  File.write('###Range of simulations parameters :\n')
  File.write('dir_simu (base name)\t' + str(option['dir_simu']) + '\n')
  File.write('num_procs\t' + str(option['num_procs']) + '\n')
  File.write('dir_sources\t' + str(option['dir_sources']) + '\n')
  File.write('dir_run\t' + str(option['dir_run']) + '\n')
  File.write('dir_init\t' + str(option['dir_init']) + '\n')
  if('eps' in option):
    File.write('###Rows of turbines parameters :\n')
    File.write('epsilon\t' + str(option['eps']) + '\n')
    File.write('r3\t' + str(option['r3']) + '\n')
    File.write('Ct_f0\t' + str(option['Ct_f0']) + '\n')
    File.write('###Rows position :\n')
    File.write('n_rows\t' + str(option['n_rows']) + '\n')
    File.write('X_f0\t' + str(option['X_f0']) + '\n')
    File.write('Y1_f0\t' + str(option['Y1_f0']) + '\n')
    File.write('Y2_f0\t' + str(option['Y2_f0']) + '\n')
    File.write('DX_f\t' + str(option['DX_f']) + '\n')
  File.write('###Forcing parameters:\n')
  File.write('omega\t' + str(option['omega']) + '\n')
  File.write('amplitude\t' + str(option['amplitude']) + '\n')
  File.write('phase_diff\t' + str(option['phi']) + '\n')
  File.write('###Numerical parameters :\n')
  File.write('pdt_init\t' + str(option['pdt_init']) + '\n')
  File.write('nsteps_init\t' + str(option['nsteps_init']) + '\n')
  File.write('n_conserve\t' + str(option['n_conserve']) + '\n')
  File.write('pdt_turb\t' + str(option['pdt_turb']) + '\n')
  File.write('nsteps_turb\t' + str(option['nsteps_turb']) + '\n')
  File.write('pdt_restart\t' + str(option['pdt_restart']) + '\n')
  File.write('nsteps_restart\t' + str(option['nsteps_restart']) + '\n')
  File.write('dt_meanP\t' + str(option['dt_meanP']) + '\n')
  File.write('marge\t' + str(option['marge']) + '\n')
  File.close()

# Deletes old version of sources, recompiles
def recompile(dir_run, dir_sources, dir_sources_ext):
  if(os.path.exists(dir_sources)):
    shutil.rmtree(dir_sources)
  shutil.copytree(dir_sources_ext, dir_sources)
  os.chdir(dir_sources)
  subprocess.call(["make", "clobber"])
  subprocess.call(["make"])
  subprocess.call(["make", "sun"])

  # Modification of Makefile in run dir:
  os.chdir(dir_run)
  replacelines('Makefile', ['SUNTANSHOME='], ['SUNTANSHOME=' + dir_sources + '\n'])
  subprocess.call(["make", "clobber"])
  subprocess.call(["make"])

# Makes a unique name for a channel configuration
def foldername(mesh, procs, phi, amp):
  return 'simu_' + mesh + '_proc' + str(procs) + '_phi' + str(phi) + '_ampl' + str(amp)

# Checks and balances for turbine configurations
# Calculates r4, r1 and Ct and records them in `option`
def turbinebookkeeping(r3, eps, X_f, Y1_f, Y2_f, option):
  if(len(X_f) != len(Y1_f) or len(X_f) != len(Y2_f)):
    print("Rows positions different lengths")
    exit()
  inter = (eps - 2 * eps * r3 + (1 - eps + pow(eps, 2)) * pow(r3, 2))
  r4 = (1 - r3 + np.sqrt(inter)) / (1 - eps)
  r1 = (r3 * (r4 + r3)) / (r4 + 2 * r3 - 1)
  Ct_f0 = eps / 2 * (pow(r4, 2) - pow(r3, 2))
  n_rows = len(X_f)

  option['eps'] = eps
  option['r1'] = r1
  option['r3'] = r3
  option['r4'] = r4
  option['Ct_f0'] = Ct_f0
  option['n_rows'] = n_rows
  option['X_f0'] = X_f
  option['Y1_f0'] = Y1_f
  option['Y2_f0'] = Y2_f
