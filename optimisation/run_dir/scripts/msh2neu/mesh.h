typedef struct
{
	long int N;		// Number of edges = number of corners
	long int *nodes;	// List of corner node numbers (size=N)
	long int *edges;	// List of edge numbers (size=N)
	double x;		// x coordinate of centroid
	double y;		// y coordinate of centroid
	double A;		// Area of this element
	long int group;		// Which group does this element belong to?
} element;

// Boundary condition types
enum {NONE, INLET, WALL, OUTLET};

typedef struct
{
	int boundary;		// True if edge is on a boundary, false if edge is internal
	long int nodes[2];	// Node numbers at the ends of this edge
	long int element1;	// The element number of one element adjacent to this edge
	long int other;		// For an internal edge this is the other adjacent element, for an external edge this is a boundary reference
	double x;		// x coordinate of mid point
	double y;		// y coordinate of mid point
	// Note - when specifying vectors, the following convention applies:
	// * The cross product of n and l is a vector pointing up off the page, ie the right hand
	//   rule applies (if n is your thumb and l is your other 4 fingers, your palm faces up off the page.)
	// * The n vector always has a positive (or zero) component in the x direction. 
	// * Where the n vector has a zero component in the x direction, it has a positive component in the y 
	//   direction. 
	double nx;		// x component of normal vector to line
	double ny;		// y component of normal vector to line
	double lx;		// x component of vector along line
	double ly;		// y component of vector along line
	double length;		// length of edge
	double scale;		// Scale of edge -  It is used as a way of converting
				// spatial derivatives to the same units as the function. 
				// This is useful for numerical purposes. 
	// The vector (ax, ay) has a magnitude equal to the length of this edge, but is a normal to the edge.
	// Its sense is pointing away from element1. (This may be thought of as the 2D equivalent of a face area
	// vector in 3D.)
	double ax;
	double ay;

	// The boundary condition type which applies to this edge. For internal edges, this is set to NONE.
	// Boundary edges must be set to one of the other types defined above.
	int boundary_condition_type; 

	// The index into an array of outlet boundary values, that applies to this edge. 
	// This only applies to edges of type OUTLET. 
	long int outlet_index;
	
	// There is now some extra information which is useful when doing basic bilinear interpolation.
	// This information is ONLY VALID FOR INTERNAL EDGES!

	double x_int;		// Coordinates of the point at the intersection of the edge and
	double y_int;		// the straight line connecting the element centroids on either side. 

	// Now it is necessary to consider a local coordinate system with its origin at (x_int, y_int)
	// and with its axes parallel to (nx, ny) and (lx, ly). In this coordinate system, let e1
	// be the element centroid with the negative value of n and e2 be the element centroid with the 
	// positive value of n. Similarly, let na be the node with the negative value of l and nb be the
	// node with the positive value of l. 
	long int e1;	// Element number of e1
	double e1n;	// and its coordinates
	double e1l;
	long int e2;	// Element number of e2
	double e2n;	// and its coordinates
	double e2l;
	long int na;	// Node number of na
	long int nb;	// Node number of nb
	double mid_l;	// l coordinate of the midpoint of the edge (the n coordinate is always zero)
} edge;

typedef struct
{
	long int num_edges;	// Number of edges connected to this node
	long int *edges;	// List of connected edges
	long int num_elements;	// Number of elements surrounding this node (for an internal node, num_edges=num_elements)
	long int *elements;	// List of surrounding elements
	double x;		// X coordinate of node
	double y;		// Y coordinate of node
	double scale;		// The local scale of the mesh around this node
	long int boundary_edges[2]; // References to the boundary edges connected to this node. A node lying on 1 or 2 boundaries will have 2 of these, whereas an internal node will have none at all (and will thus have these set to -1). 
} node;

typedef struct
{
	long int num_edges;	// Number of edges forming boundary
	long int *edges;	// List of edges forming boundary
	char *tag;		// Tag name for boundary
} boundary;

#define UNDEFINED	0
#define CONJUGATE	1
#define FLUID		2
#define POROUS		3
#define SOLID		4
#define DEFORMABLE	5

typedef struct
{
	char *tag;		// Tag name for group
	int type;		// Substance type - uses the gambit codes:
				// 	0 = Undefined (not accepted by this solver)
				// 	1 = Conjugate (not accepted by this solver)
				// 	2 = Fluid
				// 	3 = Porous (not accepted by this solver)
				// 	4 = Solid
				// 	5 = Deformable (not accepted by this solver)
	long int num_elements;	// Number of elements making up this group
	long int *elements;	// List of elements making up this group
} group;

extern long int num_nodes;
extern node *nodes;
extern long int num_elements;
extern element *elements;
extern long int num_edges;
extern edge *edges;
extern long int num_boundaries;
extern boundary *boundaries;
extern long int num_groups;
extern group *groups;

// Mesh summary properties - most extreme coordinate values (at nodes)
extern double x_min;
extern double x_max;
extern double y_min;
extern double y_max;
//Total area of mesh, and areas of the largest and smallest elements
extern double A_total;
extern double A_max;
extern double A_min;

extern double triangle_area(double x1, double y1, double x2, double y2, double x3, double y3);
extern void load_mesh(char filename[]);
extern void destroy_mesh(void);
extern void display_mesh(FILE *outfile);
extern double average_nearest_neighbour_distance(void);
extern double minimum_nearest_neighbour_distance(void);
extern long int get_num_neighbours(long int element);

extern double corner_angle(long int el, long int corner_node);
extern int inward_normal(long int ed, long int el);

extern int diamond_enclosure(double x, double y, long int ed);

