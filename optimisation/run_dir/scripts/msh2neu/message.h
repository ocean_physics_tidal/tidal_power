#define WOOPS_DATA __FILE__, __LINE__, __DATE__, __TIME__
extern void woops(char file[], long int line, char date[], char time[], char formatstring[], ...);
extern void visible_woops(char file[], long int line, char date[], char time[], char formatstring[], ...);
extern void message(char formatstring[], ...);
extern void visible_message(char formatstring[], ...);
extern void initialise_messages(void);
extern void close_messages(void);
extern void status(char formatstring[], ...);

