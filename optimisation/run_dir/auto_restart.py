#! /usr/bin/env python3.4
# Automatically re-lunch Suntans in the same directory
# (in order to access toa converged power produced)
# RQ: no change of the configuration (only change of numerical
# parameters), the computation is restarted the same folder

import shutil
import os
import sys
import numpy as np
import fileops


def auto_restart(option, dir_run, dir_sources, num_procs, dt, marge, dt_simu, nsteps):

  os.chdir(dir_run)
  while 1:
    #############Read Power file:test conv.####################
    Power = []
    Time = []
    powerfile = open('power.dat')
    for line in powerfile:
      data = line.split()
      Time.append(float(data[0]))
      Power.append(float(data[1]))
    powerfile.close()

    i0 = 0
    P_all = np.zeros(len(Power))
    n = 0
    P = []
    T_init = Time[0] - (Time[1] - Time[0])
    for i in range(len(Power)):
      P_all[n] += abs(Power[i]) * (Time[i] - T_init)
      if (Time[i] - Time[i0] >= dt):
        i0 = i
        n = n + 1
      T_init = Time[i]

    P = P_all[0: n]
    print('Power computed during the computation on a time {}: {}\n'.format(dt, P))

    if (n >= 8):
      err = abs(2 * (np.sum(P[n - 8:n - 5]) - np.sum(P[n - 4:n - 1])) / (np.sum(P[n - 8:n - 5]) + np.sum(P[n - 4:n - 1])))
    elif (n >= 4):
      err = abs(2 * (np.sum(P[n - 4:n - 3]) - np.sum(P[n - 2:n - 1])) / (np.sum(P[n - 4:n - 3]) + np.sum(P[n - 2:n - 1])))
    elif (n >= 2):
      err = abs(2 * (P[n - 2] - P[n - 1]) / (P[n - 2] + P[n - 1]))
    else:
      print('Error : not enough tide cycles to compute convergence')
      sys.exit()

    if(err < marge):
      print('The computation is converged')
      break

    # If not converged, try more steps
    # re-launch in same directory (change store in start)
    for npr in range(0, num_procs):
      shutil.copy('store.dat.' + str(npr), 'start.dat.' + str(npr))

    # Modification of the time of the simulation in suntans.dat
    fileops.replacelines('suntans.dat', ['dt ', 'nsteps '], 
        ['dt \t' + str(dt_simu) + '\t# Time step\n',
          'nsteps \t' + str(nsteps) + '\t# Time step\n'])
    fileops.runsuntans(option, dir_run, num_procs, 1)

  if (n >= 4):
    Power_final = np.sum(P[n - 4:n - 1])
  else:
    Power_final = np.sum(P[n - 2:n - 1]) * 2

  return(Power_final)

