Documentation file for msh2neu and neu2msh:
===========================================

Summary:
--------

This is the documentation file for msh2neu and neu2msh, which are tools designed for converting
between the Gambit neutral file format (*.neu) and the Fluent mesh file format (*.msh). 

Motivation:
-----------

These tools are intended to provide legacy support for CFD solvers requiring the Gambit neutral
file format for mesh input. Since Gambit is no longer supported by Ansys, it is necessary to 
provide some means for converting *.msh files (output from Icemcfd using the 
"Output to Fluent V6" command) to Gambit neutral format. This conversion may be done using msh2neu. 

The opposite conversion may be done using neu2msh, so that an existing mesh in Gambit neutral file 
format may be loaded into Icemcfd. However a *.msh file does not include the geometry that was used
to generate the mesh, so a *.msh file imported into Icemcfd is of only limited use. 

Installation:
-------------

You need gcc and make installed on your system first. 

Build the executables by typing "make" in the directory that contains the source code. 

Then it is all ready to go. If you want to be able to run these programs from any directory on your 
system, copy the executables to /bin or similar. 

Basic Usage:
------------

The command line arguments are as follows:

./msh2neu source.msh output.neu
(converts Fluent mesh file source.msh to Gambit neutral file output.neu)

./neu2msh source.neu output.msh
(converts Gambit neutral file source.neu to Fluent mesh file output.msh)

If the output file exists, it is overwritten without warning. The input file is not modified in any way.

Note that as well as performing the necessary output to the specified file, some auxilliary data
is sent to file "message". This may be safely ignored, but is useful information if an error is thrown.
If this file exists, it is overwritten without warning. 

Detailed Usage:
---------------

The intended usage of this product is for the conversion of non-periodic, two dimensional meshes.
The Fluent mesh file format supported is from Ansys IcemCFD 12.0.1 (Linux 64-bit), using the 
"Output to Fluent V6" command. The Gambit neutral format supported is as output from Gambit 2.4 
(build SP2007051420). This program may also be compatible with other versions and platforms, 
but this has not been tested. 

The Fluent mesh file should be for a valid mesh, based on a model with the following properties:

* Every curve forming a subset of the boundary of the domain must belong to a part, with the part
name corresponding to the tag associated with that section of the boundary. This tag will determine
the name of the boundary in the Gambit neutral file. A single part may consist of any number of 
connected curves, which may or may not form a closed loop. Unconnected curves may NOT be included in
the same part. Any number of parts may be used for specifying the boundary tags. 

* Curves which are internal to the domain should belong to the default part, which is GEOM.

* Points should belong to the default part, which is GEOM.

* Every surface must belong to a part, with the part name corresponding to the tag associated with that 
region of the domain. This tag will determine the name of the group in the Gambit neutral file. A single
part may consist of any number of surfaces, which may or may not be connected. Any number of parts may be
used for specifying the various domain regions. 

For example, a mesh for a simple simulation of flow around a cylinder (from left to right) could consist
of the following parts:

INLET - All the curves forming the inlet boundary, along the left side of the mesh.
OUTLET - All the curves forming the outlet boundary, along the right side of the mesh.
TOP - All the curves forming the top boundary of the mesh.
BOTTOM - All the curves forming the bottom boundary of the mesh.
CYLINDER - All the curves forming the surface of the cylinder, which acts as an "internal" boundary of 
           the mesh. These may form a closed loop. 
OUTLET_REGION - All the surfaces which form a narrow band just upstream of the outlet, where the solver
                is to use the convection-diffusion equation rather than Navier-Stokes.
DOMAIN - All the surfaces making up the rest of the domain, where Navier-Stokes is to be solved. This
         region makes up the bulk of the domain, and is the region of interest. 
GEOM - The part which includes every entity not included in any of the boundary parts or domain region
       parts. This includes the points and internal curves. 

Scope:
------

These tools are limited to the conversion of two-dimensional meshes. Features such as periodic boundaries
are not supported. 

End User License Conditions:
----------------------------

There is no warranty associated with this product, and it is provided for free on an "as is" basis. 
Use of this product is at the users own risk. This product may be modified and redistributed, as long
as the license condition and authorship information in this file is not modified or removed. 

Author:
------- 

Sam Chenoweth
samuelREMOVEchenoweth@gmail.com

(Note: I will not provide technical support; that is what this document is for. I will only provide 
bug fixes or upgrades of this software if they relate to meshes or features which I expect to need 
for my own work.)


