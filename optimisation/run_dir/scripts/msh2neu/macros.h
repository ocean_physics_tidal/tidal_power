// This file contains standard macro definitions that can be used anywhere

#define true	1
#define false	0
#define PI 3.1415926535897932384626433832795
#define E 2.7182818284590452353602874713527
#define SQ(X) ( (X) * (X) )
#define CU(X) ( (X) * (X) * (X) )

#define MIN(X,Y) (((X) < (Y) ? (X) : (Y)))
#define MAX(X,Y) (((X) > (Y) ? (X) : (Y)))

// The maximum allowable line length whenever fgets or whatever is used to read
// from a file. 
#define MAX_LINE_LENGTH 1000

// The maximum length for a string which is expected to be fairly short
#define MAX_STRING_LENGTH 1000

// Convert a string to upper case
#define ucase(STR) {long int __ucase_i;for(__ucase_i=0; __ucase_i<strlen(STR); __ucase_i++) STR[__ucase_i]=toupper(STR[__ucase_i]);}
