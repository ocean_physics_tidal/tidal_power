#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>
#include <float.h>

#include "message.h"
#include "heap.h"
#include "mesh.h"
#include "macros.h"

long int num_nodes=0;
node *nodes=NULL;

long int num_elements=0;
element *elements=NULL;

long int num_edges=0;
edge *edges=NULL;

long int num_boundaries=0;
boundary *boundaries=NULL;

long int num_groups=0;
group *groups=NULL;

double x_min;
double x_max;
double y_min;
double y_max;
double A_total;
double A_max;
double A_min;


// Calculate the area of a triangle
double triangle_area(double x1, double y1, double x2, double y2, double x3, double y3)
{
	 return fabs(0.5*(x3*y2 - x1*y2 - x3*y1 - y3*x2 + y1*x2 + y3*x1));
}

// Frees all memory associated with the mesh
void destroy_mesh(void)
{
	long int i;

	// Free the nodes
	for (i=0; i<num_nodes; i++)
	{
		if (nodes[i].edges!=NULL) sfree(nodes[i].edges, HEAP_DATA);
		if (nodes[i].elements!=NULL) sfree(nodes[i].elements, HEAP_DATA);
	}
	if (nodes!=NULL) sfree(nodes, HEAP_DATA);
	nodes=NULL;
	num_nodes=0;

	// Free the elements
	for (i=0; i<num_elements; i++)
	{
		if (elements[i].nodes!=NULL) sfree(elements[i].nodes, HEAP_DATA);
		if (elements[i].edges!=NULL) sfree(elements[i].edges, HEAP_DATA);
	}
	if (elements!=NULL) sfree(elements, HEAP_DATA);
	elements=NULL;
	num_elements=0;

	// Free the edges
	if (edges!=NULL) sfree(edges, HEAP_DATA);
	edges=NULL;
	num_edges=0;

	// Free the boundaries
	for (i=0; i<num_boundaries; i++)
	{
		if (boundaries[i].edges!=NULL) sfree(boundaries[i].edges, HEAP_DATA);
		if (boundaries[i].tag!=NULL) sfree(boundaries[i].tag, HEAP_DATA);
	}
	if (boundaries!=NULL) sfree(boundaries, HEAP_DATA);
	boundaries=NULL;
	num_boundaries=0;

	// Free the groups
	for (i=0; i<num_groups; i++)
	{
		if (groups[i].tag!=NULL) sfree(groups[i].tag, HEAP_DATA);
		if (groups[i].elements!=NULL) sfree(groups[i].elements, HEAP_DATA);
	}
	if (groups!=NULL) sfree(groups, HEAP_DATA);
	groups=NULL;
	num_groups=0;
	
	return;
}

// Calculate the areas and centroids of each of the elements
void calculate_element_properties(void)
{
	long int i;
	double x1, y1, x2, y2, x3, y3, x4, y4, xt1, yt1, xt2, yt2, A1, A2;

	for (i=0; i<num_elements; i++)
	{
		if (elements[i].N==3)
		{
			x1=nodes[elements[i].nodes[0]].x;
			y1=nodes[elements[i].nodes[0]].y;
			x2=nodes[elements[i].nodes[1]].x;
			y2=nodes[elements[i].nodes[1]].y;
			x3=nodes[elements[i].nodes[2]].x;
			y3=nodes[elements[i].nodes[2]].y;
			elements[i].A=fabs(0.5*(x3*y2 - x1*y2 - x3*y1 - y3*x2 + y1*x2 + y3*x1));
			elements[i].x=(x1+x2+x3)/3.0;
			elements[i].y=(y1+y2+y3)/3.0;
		}
		else if (elements[i].N==4)
		{
			x1=nodes[elements[i].nodes[0]].x;
			y1=nodes[elements[i].nodes[0]].y;
			x2=nodes[elements[i].nodes[1]].x;
			y2=nodes[elements[i].nodes[1]].y;
			x3=nodes[elements[i].nodes[2]].x;
			y3=nodes[elements[i].nodes[2]].y;
			x4=nodes[elements[i].nodes[3]].x;
			y4=nodes[elements[i].nodes[3]].y;
			A1=fabs(0.5*(x4*y2 - x1*y2 - x4*y1 - y4*x2 + y1*x2 + y4*x1));
			xt1=(x1+x2+x4)/3.0;
			yt1=(y1+y2+y4)/3.0;
			A2=fabs(0.5*(x4*y2 - x3*y2 - x4*y3 - y4*x2 + y3*x2 + y4*x3));
			xt2=(x3+x2+x4)/3.0;
			yt2=(y3+y2+y4)/3.0;
			elements[i].A=A1+A2;
			elements[i].x=(xt1*A1+xt2*A2)/(A1+A2);
			elements[i].y=(yt1*A1+yt2*A2)/(A1+A2);
		}
		else woops(WOOPS_DATA, "Only triangles and quadrilaterals are supported");
	}
	
	return;
}

// Find an edge defined by a pair of nodes in the edge table. 
// Returns -1 if the edge is not found. 
long int find_edge(long int node1, long int node2)
{
	long int i;
	long int ed;

	if (node1==node2 || node1<0 || node1>=num_nodes || node2<0 || node2>=num_nodes) woops(WOOPS_DATA, "Invalid edge");

	for (i=0; i<nodes[node1].num_edges; i++)
	{
		ed=nodes[node1].edges[i];

		if ((edges[ed].nodes[0]==node1 && edges[ed].nodes[1]==node2) || (edges[ed].nodes[0]==node2 && edges[ed].nodes[1]==node1)) return ed;
	}

	return -1;
}

// Add an edge defined by a pair of nodes to the edge table. The edge may already exist in the table,
// in which case it is not added. Either way, the edge number of the corresponding edge is returned.
long int add_edge(long int node1, long int node2, long int elem)
{
	long int ed;

	if (node1==node2 || node1<0 || node1>=num_nodes || node2<0 || node2>=num_nodes) woops(WOOPS_DATA, "Invalid edge");

	// Check the current edge table first
	ed=find_edge(node1, node2);
	if (ed>=0 && ed<num_edges)
	{
		if (edges[ed].other>=0) woops(WOOPS_DATA, "Edge detected which is common to 3 elements/boundaries");
		edges[ed].boundary=false;
		edges[ed].other=elem;

		return ed;
	}

	// If not found, add a new edge
	num_edges++;
	edges=(edge *)srealloc((void *)edges,sizeof(edge)*num_edges, HEAP_DATA);

	edges[num_edges-1].boundary=true;
	edges[num_edges-1].nodes[0]=node1;
	edges[num_edges-1].nodes[1]=node2;
	edges[num_edges-1].element1=elem;
	edges[num_edges-1].other=-1;
	edges[num_edges-1].x=0.5*(nodes[node1].x+nodes[node2].x);
	edges[num_edges-1].y=0.5*(nodes[node1].y+nodes[node2].y);
	edges[num_edges-1].length=sqrt(SQ(nodes[node1].x-nodes[node2].x)+SQ(nodes[node1].y-nodes[node2].y));
	edges[num_edges-1].lx=(nodes[node1].x-nodes[node2].x)/edges[num_edges-1].length;
	edges[num_edges-1].ly=(nodes[node1].y-nodes[node2].y)/edges[num_edges-1].length;
	edges[num_edges-1].nx=-edges[num_edges-1].ly;
	edges[num_edges-1].ny=edges[num_edges-1].lx;

	// Fix the sign convention on the vectors
	if (edges[num_edges-1].nx<0.0)
	{
		edges[num_edges-1].nx*=-1.0;
		edges[num_edges-1].ny*=-1.0;
	}
	else if (edges[num_edges-1].nx>0.0)
	{
		// Do not delete this else if - it is fundamental to the logic
	}
	else if (edges[num_edges-1].ny<0.0)
	{
		edges[num_edges-1].nx*=-1.0;
		edges[num_edges-1].ny*=-1.0;		
	}
	if (edges[num_edges-1].nx*edges[num_edges-1].ly-edges[num_edges-1].ny*edges[num_edges-1].lx<0.0)
	{
		edges[num_edges-1].lx*=-1.0;
		edges[num_edges-1].ly*=-1.0;
	}

	// Calculate the (ax, ay) vector
	edges[num_edges-1].ax=nodes[node2].y-nodes[node1].y;
	edges[num_edges-1].ay=nodes[node1].x-nodes[node2].x;
	if (edges[num_edges-1].ax*(edges[num_edges-1].x-elements[edges[num_edges-1].element1].x) + edges[num_edges-1].ay*(edges[num_edges-1].y-elements[edges[num_edges-1].element1].y)<0.0)
	{
		edges[num_edges-1].ax*=-1.0;
		edges[num_edges-1].ay*=-1.0;
	}
	
	return num_edges-1;
}

// Adds a reference to an element to a given node
void link_element_to_node(long int elem, long int node)
{
	long int i;

	if (elem<0 || elem>=num_elements) woops(WOOPS_DATA, "Invalid element number");
	if (node<0 || node>=num_nodes) woops(WOOPS_DATA, "Invalid node number");
	
	for (i=0; i<nodes[node].num_elements; i++)
	{
		if (nodes[node].elements[i]==elem) return;
	}

	nodes[node].num_elements++;
	nodes[node].elements=(long int *)srealloc((void *)nodes[node].elements, sizeof(long int)*nodes[node].num_elements, HEAP_DATA);
	nodes[node].elements[nodes[node].num_elements-1]=elem;
	
	return;
}

// Adds a reference to an edge to a given node
void link_edge_to_node(long int edge, long int node)
{
	long int i;

	if (edge<0 || edge>=num_edges) woops(WOOPS_DATA, "Invalid edge number");
	if (node<0 || node>=num_nodes) woops(WOOPS_DATA, "Invalid node number");

	for (i=0; i<nodes[node].num_edges; i++)
	{
		if (nodes[node].edges[i]==edge) return;
	}
	
	nodes[node].num_edges++;
	nodes[node].edges=(long int *)srealloc((void *)nodes[node].edges, sizeof(long int)*nodes[node].num_edges, HEAP_DATA);
	nodes[node].edges[nodes[node].num_edges-1]=edge;
	
	return;
}

// Adds a boundary reference to a given edge
void link_boundary_to_edge(long int boundary, long int edge)
{
	if (boundary<0 || boundary>=num_boundaries) woops(WOOPS_DATA, "Invalid boundary");
	if (edge<0 || edge>=num_edges) woops(WOOPS_DATA, "Invalid edge");

	if (edges[edge].boundary==false) woops(WOOPS_DATA, "Attempt to assign a boundary to an internal edge");
	edges[edge].other=boundary;
	
	return;
}

// Generate the edge table information and update the node and element tables to reference this.
void generate_edges(void)
{
	long int i;
	long int edge_number;
	long int node1, node2, node3, node4;

	for (i=0; i<num_elements; i++)
	{
		if (elements[i].N==3)
		{
			node1=elements[i].nodes[0];
			node2=elements[i].nodes[1];
			node3=elements[i].nodes[2];

			edge_number=add_edge(node1,node2,i);
			elements[i].edges[0]=edge_number;
			link_edge_to_node(edge_number, node1);
			link_edge_to_node(edge_number, node2);
			
			edge_number=add_edge(node2,node3,i);
			elements[i].edges[1]=edge_number;
			link_edge_to_node(edge_number, node2);
			link_edge_to_node(edge_number, node3);
			
			edge_number=add_edge(node3,node1,i);
			elements[i].edges[2]=edge_number;
			link_edge_to_node(edge_number, node3);
			link_edge_to_node(edge_number, node1);

			link_element_to_node(i, node1);
			link_element_to_node(i, node2);
			link_element_to_node(i, node3);
		}
		else if (elements[i].N==4)
		{
			node1=elements[i].nodes[0];
			node2=elements[i].nodes[1];
			node3=elements[i].nodes[2];
			node4=elements[i].nodes[3];

			edge_number=add_edge(node1,node2,i);
			elements[i].edges[0]=edge_number;
			link_edge_to_node(edge_number, node1);
			link_edge_to_node(edge_number, node2);

			edge_number=add_edge(node2,node3,i);
			elements[i].edges[1]=edge_number;
			link_edge_to_node(edge_number, node2);
			link_edge_to_node(edge_number, node3);
			
			edge_number=add_edge(node3,node4,i);
			elements[i].edges[2]=edge_number;
			link_edge_to_node(edge_number, node3);
			link_edge_to_node(edge_number, node4);
			
			edge_number=add_edge(node4,node1,i);
			elements[i].edges[3]=edge_number;
			link_edge_to_node(edge_number, node4);
			link_edge_to_node(edge_number, node1);

			link_element_to_node(i, node1);
			link_element_to_node(i, node2);
			link_element_to_node(i, node3);
			link_element_to_node(i, node4);
		}
		else woops(WOOPS_DATA, "Only triangles and quadrilaterals are supported");
	}
	
	return;
}

// Calculate the local scale parameter for each edge.
// This is used to allow a reasonable numerical comparison between spatial derivatives and
// function values. 
void calculate_edge_scales()
{
	long int i;
	double A1, A2, d1, d2;

	for (i=0; i<num_edges; i++)
	{
		if (edges[i].boundary==true)
		{
			A1=elements[edges[i].element1].A;
			d1=sqrt(A1);
			edges[i].scale=d1;
		}
		else
		{
			A1=elements[edges[i].element1].A;
			d1=sqrt(A1);
			A2=elements[edges[i].other].A;
			d2=sqrt(A2);
			edges[i].scale=sqrt(d1*d2);
		}
	}
	
	return;
}

// Displays the mesh data
void display_mesh(FILE *outfile)
{
	long int i, ii;
	char typestr[MAX_STRING_LENGTH+1];

	fprintf(outfile, "Global x range: %lf to %lf\n", x_min, x_max);
	fprintf(outfile, "Global y range: %lf to %lf\n", y_min, y_max);
	fprintf(outfile, "Total Mesh Area: %lf\n\n", A_total);
	fprintf(outfile, "Area of Smallest Element: %lf\n\n", A_min);
	fprintf(outfile, "Area of Largest Element: %lf\n\n", A_max);

	for (i=0; i<num_elements; i++)
	{
		fprintf(outfile, "Element %ld: %ld edges/corners\n", i, elements[i].N);
		fprintf(outfile, "\tArea=%lf, Centroid=(%lf, %lf)\n", elements[i].A, elements[i].x, elements[i].y);
		fprintf(outfile, "\tCorner Nodes={");
		if (elements[i].N>0) fprintf(outfile, "%ld", elements[i].nodes[0]);
		for (ii=1; ii<elements[i].N; ii++) fprintf(outfile, ", %ld", elements[i].nodes[ii]);
		fprintf(outfile, "}\n");
		fprintf(outfile, "\tEdges={");
		if (elements[i].N>0) fprintf(outfile, "%ld", elements[i].edges[0]);
		for (ii=1; ii<elements[i].N; ii++) fprintf(outfile, ", %ld", elements[i].edges[ii]);
		fprintf(outfile, "}\n");
		fprintf(outfile, "\tBelongs to group %ld - \"%s\"\n\n", elements[i].group, groups[elements[i].group].tag);
	}

	for (i=0; i<num_nodes; i++)
	{
		if (nodes[i].boundary_edges[0]<0 && nodes[i].boundary_edges[1]<0)
		{
			sprintf(typestr, "Node is interior");
		}
		else if (nodes[i].boundary_edges[0]>=0 && nodes[i].boundary_edges[1]>=0)
		{
			sprintf(typestr, "Edge %ld on boundary \"%s\",", nodes[i].boundary_edges[0], boundaries[edges[nodes[i].boundary_edges[0]].other].tag);
			sprintf(typestr, "%s Edge %ld on boundary \"%s\"", typestr, nodes[i].boundary_edges[1], boundaries[edges[nodes[i].boundary_edges[1]].other].tag);
		}
		else woops(WOOPS_DATA, "Corrupt boundary reference data for node %ld", i);

		fprintf(outfile, "Node %ld:\n", i);
		fprintf(outfile, "\tLocation=(%lf, %lf)\n", nodes[i].x, nodes[i].y);
		fprintf(outfile, "\tLocal Mesh Scale=%lf\n", nodes[i].scale);
		fprintf(outfile, "\tConnecting Edges={");
		if (nodes[i].num_edges>0) fprintf(outfile, "%ld", nodes[i].edges[0]);
		for (ii=1; ii<nodes[i].num_edges; ii++) fprintf(outfile, ", %ld", nodes[i].edges[ii]);
		fprintf(outfile, "}\n");
		fprintf(outfile, "\tAdjacent Elements={");
		if (nodes[i].num_elements>0) fprintf(outfile, "%ld", nodes[i].elements[0]);
		for (ii=1; ii<nodes[i].num_elements; ii++) fprintf(outfile, ", %ld", nodes[i].elements[ii]);
		fprintf(outfile, "}\n");
		fprintf(outfile, "\t%s\n\n", typestr);
	}

	for (i=0; i<num_edges; i++)
	{
		fprintf(outfile, "Edge %ld (%s):\n", i, (edges[i].boundary==true ? "Boundary" : "Interior"));
		fprintf(outfile, "\tMidpoint=(%lf, %lf)\n", edges[i].x, edges[i].y);
		fprintf(outfile, "\tEdge Length = %lf\n", edges[i].length);
		fprintf(outfile, "\tEdge Scale = %lf\n", edges[i].scale);
		fprintf(outfile, "\tNormal Vector=(%lf, %lf)\n", edges[i].nx, edges[i].ny);
		fprintf(outfile, "\tLine Vector=(%lf, %lf)\n", edges[i].lx, edges[i].ly);
		fprintf(outfile, "\tArea Vector (normal, with magnitude equal to length of edge)=(%lf, %lf)\n", edges[i].ax, edges[i].ay);
		fprintf(outfile, "\tEnd Nodes={%ld, %ld}\n", edges[i].nodes[0], edges[i].nodes[1]);
		if (edges[i].boundary==true) fprintf(outfile, "\tAdjacent element={%ld}, Adjacent boundary={%ld}\n", edges[i].element1, edges[i].other);
		else fprintf(outfile, "\tAdjacent elements={%ld, %ld}\n", edges[i].element1, edges[i].other);
		if (edges[i].boundary==false)
		{
			fprintf(outfile, "\tCoordinates of intersection point=(%lf,%lf)\n", edges[i].x_int, edges[i].y_int);
			fprintf(outfile, "\tElement e1=%ld (n=%lf,l=%lf)\n", edges[i].e1, edges[i].e1n, edges[i].e1l);
			fprintf(outfile, "\tElement e2=%ld (n=%lf,l=%lf)\n", edges[i].e2, edges[i].e2n, edges[i].e2l);
			fprintf(outfile, "\tNode na=%ld, nb=%ld\n", edges[i].na, edges[i].nb);
			fprintf(outfile, "\tMidpoint at l=%lf\n", edges[i].mid_l);
		}
		fprintf(outfile, "\n");
	}

	for (i=0; i<num_boundaries; i++)
	{
		fprintf(outfile, "Boundary %ld - \"%s\":\n", i, boundaries[i].tag);
		fprintf(outfile, "\tEdges={");
		if (boundaries[i].num_edges>0) fprintf(outfile, "%ld", boundaries[i].edges[0]);
		for (ii=1; ii<boundaries[i].num_edges; ii++) fprintf(outfile, ", %ld", boundaries[i].edges[ii]);
		fprintf(outfile, "}\n\n");
	}

	for (i=0; i<num_groups; i++)
	{
		fprintf(outfile, "Group %ld - \"%s\":\n", i, groups[i].tag);
		if (groups[i].type==FLUID) strcpy(typestr, "FLUID");
		else if (groups[i].type==SOLID) strcpy(typestr, "SOLID");
		else strcpy(typestr, "<Unsupported Rheology>");
		fprintf(outfile, "\tType: %s\n", typestr);
		fprintf(outfile, "\tNumber of Elements in Group = %ld\n", groups[i].num_elements);
		fprintf(outfile, "\tElements in Group={");
		for (ii=0; ii<groups[i].num_elements; ii++) 
		{
			if (ii<groups[i].num_elements-1) fprintf(outfile, "%ld, ", groups[i].elements[ii]);
			else fprintf(outfile, "%ld}\n\n", groups[i].elements[ii]);
		}
	}

	return;
}

// Temporary structure used for processing the list of boundary nodes
// to make a list of sequential edges
typedef struct
{
	long int node_num;		// Number of a node (range: 0 to num_nodes-1)
	long int connectivity_count;	// The number of boundary nodes in the list
					// that are connected to this node (range: 1 or 2)
	long int connecting_nodes[2];	// List of the nodes numbers of connecting nodes
	long int connecting_edges[2];	// List of the edge numbers of connecting edges,
					// in the same order as their corresponding nodes
	long int connecting_refs[2];	// List of connectivity table indexes to the connecting nodes
	int marked;			// Has this node been visited already by the boundary
					// searching algorithm?
} connectivity;

// Comparison function for sorting connectivity items
int cmp_connectivity(connectivity *c1,  connectivity *c2)
{
	return (c1->node_num)-(c2->node_num);
}

// Uses a binary search algorithm to find a connectivity entry with node number equal to node_num. 
// The algorithm searches in the connectivity table connections, between indexes of lower_limit and upper_limit.
// The return value is the index of the desired entry. 
long int find_connection(long int node_num, connectivity *connections, long int lower_limit, long int upper_limit)
{
	long int mid;
	
	while (lower_limit < upper_limit)
	{
		mid=(lower_limit+upper_limit)/2;
		if (node_num < connections[mid].node_num)
		{
			upper_limit=mid-1;
		}
		else if (node_num > connections[mid].node_num)
		{
			lower_limit=mid+1;
		}
		else
		{
			lower_limit=mid;
			upper_limit=mid;
		}
	}

	if (lower_limit==upper_limit) return lower_limit;
	else woops(WOOPS_DATA, "Node misconnection (%ld)", node_num);
}

// Loads a mesh file (Gambit neutral file *.neu) and builds the necessary data structures
void load_mesh(char filename[])
{
	FILE *fp;
	char line[MAX_LINE_LENGTH+1];
	long int NUMNP, NELEM, NGRPS, NBSETS, NDFCD, NDFVL;
	long int i, ii, iii;
	long int N, NTYPE, NDP, CORNER;
	double x, y;
	char tag[MAX_STRING_LENGTH+1];
	char c;
	long int end;
	long int ITYPE, NENTRY, NVALUES, IBCODE1;
	long int node_num, last_node_num, end_node_num, edge_num;
	long int *bnodes;
	connectivity *connections;
	long int found_edge, current_ref, old_ref, lower_limit, upper_limit;
	long int char_count;
	long int GRP, ELEM, MAT, NFLGS;
	char bnodes_str[MAX_STRING_LENGTH+1];
	double x1, y1, x2, y2, xa, ya, xb, yb, qn, det;

	fp=fopen(filename, "r");
	if (fp==NULL) woops(WOOPS_DATA, "Could not open mesh file \"%s\"", filename);

	// Read the control info section
	fgets(line, MAX_LINE_LENGTH, fp);
	if (strncmp(line, "        CONTROL INFO", strlen("        CONTROL INFO"))!=0)
		woops(WOOPS_DATA, "Could not find control info in file \"%s\"", filename);

	// Read and check the next line
	fgets(line, MAX_LINE_LENGTH, fp);
	if (strncmp(line, "** GAMBIT NEUTRAL FILE", strlen("** GAMBIT NEUTRAL FILE"))!=0) woops(WOOPS_DATA, "Mesh file \"%s\" is not a Gambit Neutral File", filename);

	// Read and discard a few lines
	fgets(line, MAX_LINE_LENGTH, fp);
	fgets(line, MAX_LINE_LENGTH, fp);
	fgets(line, MAX_LINE_LENGTH, fp);

	// Read and check the next line
	fgets(line, MAX_LINE_LENGTH, fp);
	if (strncmp(line, "     NUMNP     NELEM     NGRPS    NBSETS     NDFCD     NDFVL", strlen("     NUMNP     NELEM     NGRPS    NBSETS     NDFCD     NDFVL"))!=0) woops(WOOPS_DATA, "Mesh file \"%s\" does not have the correct format", filename);
	
	// Read the totals
	fgets(line, MAX_LINE_LENGTH, fp);
	sscanf(line, "%ld%ld%ld%ld%ld%ld", &NUMNP, &NELEM, &NGRPS, &NBSETS, &NDFCD, &NDFVL);
	if (NDFCD!=2 || NDFVL!=2) woops(WOOPS_DATA, "Mesh file \"%s\" is not a 2D mesh", filename);

	// Allocate the data structures whose size is now known
	num_nodes=NUMNP;
	nodes=(node *)smalloc(sizeof(node)*num_nodes, HEAP_DATA);

	num_elements=NELEM;
	elements=(element *)smalloc(sizeof(element)*num_elements, HEAP_DATA);

	num_boundaries=NBSETS;
	boundaries=(boundary *)smalloc(sizeof(boundary)*num_boundaries, HEAP_DATA);

	num_groups=NGRPS;
	groups=(group *)smalloc(sizeof(group)*num_groups, HEAP_DATA);

	// Read the end of section line
	fgets(line, MAX_LINE_LENGTH, fp);
	if (strncmp(line, "ENDOFSECTION", strlen("ENDOFSECTION"))!=0) woops(WOOPS_DATA, "End of section not found in file \"%s\"", filename);
	
	// Read through the file until the nodal coordinates section is reached.
	// This could mean reading and discarding the entire application data section, if
	// it is present. 
	fgets(line, MAX_LINE_LENGTH, fp);
	if (strncmp(line, "    APPLICATION DATA", strlen("    APPLICATION DATA"))==0)
	{
		do
		{
			fgets(line, MAX_LINE_LENGTH, fp);
		} while (strcmp(line, "ENDOFSECTION\n")!=0);
		fgets(line, MAX_LINE_LENGTH, fp);
	}
	
	// Read the node section
	if (strncmp(line, "   NODAL COORDINATES", strlen("   NODAL COORDINATES"))!=0) woops(WOOPS_DATA, "Could not find node table in file \"%s\"", filename);
	message("\t\tReading node data...\n");

	// Read in the node coordinates
	for (i=0; i<num_nodes; i++)
	{
		fgets(line, MAX_LINE_LENGTH, fp);
		sscanf(line, "%ld%lf%lf", &N, &x, &y);
		if (N!=i+1) woops(WOOPS_DATA, "Corrupt node table in file \"%s\"", filename);
		nodes[i].x=x;
		nodes[i].y=y;
		nodes[i].num_edges=0;
		nodes[i].edges=NULL;
		nodes[i].num_elements=0;
		nodes[i].elements=NULL;
		nodes[i].boundary_edges[0]=-1;
		nodes[i].boundary_edges[1]=-1;
	}

	// Read the end of section line
	fgets(line, MAX_LINE_LENGTH, fp);
	if (strncmp(line, "ENDOFSECTION", strlen("ENDOFSECTION"))!=0) woops(WOOPS_DATA, "End of section not found in file \"%s\"", filename);
	
	// Read the elements section
	fgets(line, MAX_LINE_LENGTH, fp);
	if (strncmp(line, "      ELEMENTS/CELLS", strlen("      ELEMENTS/CELLS"))!=0)
		woops(WOOPS_DATA, "Could not find element table in file \"%s\"", filename);
	message("\t\tReading element data...\n");

	// Read in the elements
	for (i=0; i<num_elements; i++)
	{
		fscanf(fp, "%ld%ld%ld", &N, &NTYPE, &NDP);
		if (N!=i+1) woops(WOOPS_DATA, "Corrupt element table in file \"%s\"", filename);
		if (NTYPE!=2 && NTYPE!=3) woops(WOOPS_DATA, "Can only support triangular or quadrilateral elements");
		if (NTYPE==2 && NDP!=4) woops(WOOPS_DATA, "Quadrilateral elements require 4 nodes");
		if (NTYPE==3 && NDP!=3) woops(WOOPS_DATA, "Triangular elements require 3 nodes");
		elements[i].N=NDP;
		
		elements[i].nodes=(long int *)smalloc(sizeof(long int)*elements[i].N, HEAP_DATA);

		elements[i].edges=(long int *)smalloc(sizeof(long int)*elements[i].N, HEAP_DATA);

		// Set the elements group membership to invalid
		elements[i].group=-1;

		x=0;
		y=0;
		for (ii=0; ii<elements[i].N; ii++)
		{
			fscanf(fp, "%ld", &CORNER);
			elements[i].nodes[ii]=CORNER-1;
			if (elements[i].nodes[ii]<0 || elements[i].nodes[ii]>=num_nodes) woops(WOOPS_DATA, "Invalid corner node (Element=%ld, Corner Node=%ld, num_nodes=%ld)", i, elements[i].nodes[ii], num_nodes);
		}

		// Read the remainder of the line
		fgets(line, MAX_LINE_LENGTH, fp);
	}

	// Read the end of section line
	fgets(line, MAX_LINE_LENGTH, fp);
	if (strncmp(line, "ENDOFSECTION", strlen("ENDOFSECTION"))!=0) woops(WOOPS_DATA, "End of section not found in file \"%s\"", filename);

	// Process the element data to calculate the areas and generate the edge table, etc.
	message("\t\tCalculating element properties...\n");
	calculate_element_properties();
	message("\t\tGenerating edges...\n");
	generate_edges();
	calculate_edge_scales();	
	
	// Read the group section for each group
	message("\t\tReading group definitions...\n");
	for (i=0; i<num_groups; i++)
	{
		fgets(line, MAX_LINE_LENGTH, fp);
		if (strncmp(line, "       ELEMENT GROUP", strlen("       ELEMENT GROUP"))!=0) woops(WOOPS_DATA, "Element Group information not present in file \"%s\"", filename);
		fgets(line, MAX_LINE_LENGTH, fp);
		sscanf(line, "GROUP:%ld ELEMENTS:%ld MATERIAL:%ld NFLAGS:%ld", &GRP, &ELEM, &MAT, &NFLGS);
		if (GRP!=i+1) woops(WOOPS_DATA, "Missing group record");
		if (MAT!=FLUID && MAT!=SOLID) woops(WOOPS_DATA, "Only solid or fluid media are supported by this solver");
		if (ELEM<1) woops(WOOPS_DATA, "Every group must contain at least 1 element");
		
		groups[i].type=MAT;
		groups[i].num_elements=ELEM;
		groups[i].elements=(long int *)smalloc(sizeof(long int)*ELEM, HEAP_DATA);

		char_count=0;
		do
		{
			c=(char) fgetc(fp);
			char_count++;
		} while (isspace(c));
		sprintf(tag, "%c", c);
		do
		{
			c=(char) fgetc(fp);
			char_count++;
			if (char_count<=32)
			{
				end=strlen(tag);
				tag[end]=c;
				tag[end+1]='\0';
			}
		} while (char_count<32);
		groups[i].tag=(char *)smalloc(sizeof(char)*(strlen(tag)+1), HEAP_DATA);
		ucase(tag);
		strcpy(groups[i].tag, tag);

		// Read the rest of the line
		fgets(line, MAX_LINE_LENGTH, fp);
		
		// Read the line containing all the solver dependent flags
		fgets(line, MAX_LINE_LENGTH, fp);
		
		// Read in the elements belonging to the group
		for (ii=0; ii<groups[i].num_elements; ii++)
		{
			fscanf(fp, " %ld", &(groups[i].elements[ii]));
			groups[i].elements[ii]--;

			// Update the group reference in the element table
			if (elements[groups[i].elements[ii]].group<0) elements[groups[i].elements[ii]].group=i;
			else woops(WOOPS_DATA, "Element detected which belongs to more than 1 group (Element %ld belongs to (at least) groups %ld and %ld)", groups[i].elements[ii], elements[groups[i].elements[ii]].group, i);
		}
		
		// Get the rest of the line
		fgets(line, MAX_LINE_LENGTH, fp);
		
		// Get the end of section
		fgets(line, MAX_LINE_LENGTH, fp);
		if (strncmp(line, "ENDOFSECTION", strlen("ENDOFSECTION"))!=0) woops(WOOPS_DATA, "End of section not found in file \"%s\"", filename);
	}

	// Read the boundary condition header for each boundary
	message("\t\tReading boundary definitions...\n");
	for (i=0; i<num_boundaries; i++)
	{
		// Read the boundary section
		fgets(line, MAX_LINE_LENGTH, fp);
		if (strncmp(line, " BOUNDARY CONDITIONS", strlen(" BOUNDARY CONDITIONS"))!=0) woops(WOOPS_DATA, "Could not find boundary table %ld in file \"%s\"", i, filename);

		char_count=0;
		do
		{
			c=(char) fgetc(fp);
			char_count++;
		} while (isspace(c));
		sprintf(tag, "%c", c);
		do
		{
			c=(char) fgetc(fp);
			char_count++;
			if (char_count<=32)
			{
				end=strlen(tag);
				tag[end]=c;
				tag[end+1]='\0';
			}
		} while (char_count<32);

		message("\t\t\t%s\n", tag);

		fscanf(fp, "%ld%ld%ld%ld", &ITYPE, &NENTRY, &NVALUES, &IBCODE1);
		if (ITYPE!=0) woops(WOOPS_DATA, "Boundaries need to be defined in terms of nodes, not elements. Gambit has an option to let you change this.");
		if (NVALUES!=0) woops(WOOPS_DATA, "Boundary sets should specify the definition of the boundaries but not the conditions which apply at the boundaries. The actual boundary conditions should be specified in the code. This allows time dependent boundary conditions, coupled boundary conditions, etc.");
		if (IBCODE1!=24) woops(WOOPS_DATA, "Boundaries should be specified using tags linked to NODEs, not using standard Fluent boundary concepts such as OUTFLOW, WALL, etc.");
		if (NENTRY<2) woops(WOOPS_DATA, "Boundaries must contain at least 2 nodes");
		// Get the rest of the line
		fgets(line, MAX_LINE_LENGTH, fp);

		boundaries[i].num_edges=0;
		boundaries[i].edges=NULL;
		boundaries[i].tag=(char *)smalloc(sizeof(char)*(strlen(tag)+1), HEAP_DATA);
		ucase(tag);
		strcpy(boundaries[i].tag, tag);

		// Read in the boundary nodes in the order that they are given. 
		// The first node is always an end node, subsequent nodes are the end nodes of the rest of the
		// geometrical edges and the rest of the nodes are given last. 
		// The safest way to deal with this is to use our knowledge of the connectivity of the mesh to find the
		// (hopefully unique) boundary edge connecting one of the remaining nodes in the list to the first node.
		// Having found this node, it is then possible to repeat the process to find the next boundary edge. 
		// Important Note: Although this technique supports boundaries containing multiple geometry edges,
		// it does NOT support discontinuous boundaries: the geometry edges associated with a tag must all
		// be connected together. 
		bnodes=(long int *)smalloc(sizeof(long int)*NENTRY, HEAP_DATA);
		connections=(connectivity *)smalloc(sizeof(connectivity)*NENTRY, HEAP_DATA);
		for (ii=0; ii<NENTRY; ii++)
		{
			fscanf(fp, "%ld", &node_num);
			node_num--;
			bnodes[ii]=node_num;
		}

		// Process the list of boundary nodes to make a connectivity table
		for (ii=0; ii<NENTRY; ii++)
		{
			connections[ii].node_num=bnodes[ii];
			connections[ii].connectivity_count=0;
			connections[ii].connecting_nodes[0]=-1;
			connections[ii].connecting_nodes[1]=-1;
			connections[ii].connecting_edges[0]=-1;
			connections[ii].connecting_edges[1]=-1;
			connections[ii].connecting_refs[0]=-1;
			connections[ii].connecting_refs[1]=-1;
			connections[ii].marked=false;

			for (iii=0; iii<NENTRY; iii++)
			{
				if (ii!=iii)
				{
					found_edge=find_edge(bnodes[ii], bnodes[iii]);
					if (found_edge>=0 && found_edge<num_edges)
					{
						if (edges[found_edge].boundary==true)
						{
							if (connections[ii].connectivity_count<2 && connections[ii].connectivity_count>=0)
							{
								connections[ii].connecting_nodes[connections[ii].connectivity_count]=bnodes[iii];
								connections[ii].connecting_edges[connections[ii].connectivity_count]=found_edge;
							}
							connections[ii].connectivity_count++;
						}
					}
				}
			}
		}

		// Check the connectivity table for invalid data
		for (ii=0; ii<NENTRY; ii++)
		{
			if (connections[ii].connectivity_count<0) woops(WOOPS_DATA, "Programmer screwed up");
			if (connections[ii].connectivity_count==0) woops(WOOPS_DATA, "Unconnected node in boundary \"%s\"", boundaries[i].tag);
			if (connections[ii].connectivity_count>2) woops(WOOPS_DATA, "Loopback in boundary \"%s\"", boundaries[i].tag);
		}

		// Sort the connectivity table by node number
		qsort((void *)connections, NENTRY, sizeof(connectivity), (void *)cmp_connectivity);

		// Generate the quick reference links for the connectivity table
		for (ii=0; ii<NENTRY; ii++)
		{
			if (connections[ii].connecting_nodes[0]<connections[ii].node_num)
			{
				lower_limit=0;
				upper_limit=ii-1;
			}
			else if (connections[ii].connecting_nodes[0]>connections[ii].node_num)
			{
				lower_limit=ii+1;
				upper_limit=NENTRY-1;
			}
			if (connections[ii].connectivity_count>0) connections[ii].connecting_refs[0]=find_connection(connections[ii].connecting_nodes[0], connections, lower_limit, upper_limit);

			if (connections[ii].connecting_nodes[1]<connections[ii].node_num)
			{
				lower_limit=0;
				upper_limit=ii-1;
			}
			else if (connections[ii].connecting_nodes[1]>connections[ii].node_num)
			{
				lower_limit=ii+1;
				upper_limit=NENTRY-1;
			}
			if (connections[ii].connectivity_count>1) connections[ii].connecting_refs[1]=find_connection(connections[ii].connecting_nodes[1], connections, lower_limit, upper_limit);
		}
		
		/*
		// This block of code is for debugging purposes only. It can be used to examine the contents of the connectivity table.
		
		printf("%s:\n", boundaries[i].tag);
		for (ii=0; ii<NENTRY; ii++)
		{
			printf("Node: %ld, Connectivity: %ld, Nodes Connected: %ld %ld, Edges Connected: %ld %ld, Refs: %ld %ld\n", connections[ii].node_num, connections[ii].connectivity_count, connections[ii].connecting_nodes[0], connections[ii].connecting_nodes[1], connections[ii].connecting_edges[0], connections[ii].connecting_edges[1], connections[ii].connecting_refs[0], connections[ii].connecting_refs[1]);
		}
		*/

		// Search the connectivity table to find non-loop edges
		for (ii=0; ii<NENTRY; ii++)
		{
			// If an unmarked end is discovered...
			if (connections[ii].connectivity_count==1 && connections[ii].marked==false)
			{
				// ... trace its connectivity
				current_ref=ii;
				old_ref=-1;
				do
				{
					// Add the next edge
					boundaries[i].num_edges++;
					boundaries[i].edges=(long int *)srealloc((void *)boundaries[i].edges, sizeof(long int)*boundaries[i].num_edges, HEAP_DATA);
					
					if (old_ref<0) boundaries[i].edges[boundaries[i].num_edges-1]=connections[current_ref].connecting_edges[0];
					else if (connections[current_ref].connecting_refs[0]==old_ref) boundaries[i].edges[boundaries[i].num_edges-1]=connections[current_ref].connecting_edges[1];
					else if (connections[current_ref].connecting_refs[1]==old_ref) boundaries[i].edges[boundaries[i].num_edges-1]=connections[current_ref].connecting_edges[0];
					else woops(WOOPS_DATA, "Programmer screwed up");
					
					// Link the boundary to the edge
					link_boundary_to_edge(i, boundaries[i].edges[boundaries[i].num_edges-1]);
					
					// Mark the first node as visited
					connections[current_ref].marked=true;
					
					// Proceed to the next node
					if (old_ref<0) 
					{
						old_ref=current_ref;
						current_ref=connections[current_ref].connecting_refs[0];
					}
					else if (connections[current_ref].connecting_refs[0]==old_ref)
					{
						old_ref=current_ref;
						current_ref=connections[current_ref].connecting_refs[1];
					}
					else if (connections[current_ref].connecting_refs[1]==old_ref)
					{
						old_ref=current_ref;
						current_ref=connections[current_ref].connecting_refs[0];
					}
					else woops(WOOPS_DATA, "Programmer screwed up");
					
				} while (connections[current_ref].connectivity_count==2);
				connections[current_ref].marked=true;
			}
		}
		
		// Search the connectivity table to find loop edges
		for (ii=0; ii<NENTRY; ii++)
		{
			// If an unmarked node is discovered...
			if (connections[ii].marked==false)
			{
				// ... trace its connectivity
				current_ref=ii;
				old_ref=-1;
				do
				{
					// Add the next edge
					boundaries[i].num_edges++;
					boundaries[i].edges=(long int *)srealloc((void *)boundaries[i].edges, sizeof(long int)*boundaries[i].num_edges, HEAP_DATA);
					
					if (old_ref<0) boundaries[i].edges[boundaries[i].num_edges-1]=connections[current_ref].connecting_edges[0];
					else if (connections[current_ref].connecting_refs[0]==old_ref) boundaries[i].edges[boundaries[i].num_edges-1]=connections[current_ref].connecting_edges[1];
					else if (connections[current_ref].connecting_refs[1]==old_ref) boundaries[i].edges[boundaries[i].num_edges-1]=connections[current_ref].connecting_edges[0];
					else woops(WOOPS_DATA, "Programmer screwed up");
					
					// Link the boundary to the edge
					link_boundary_to_edge(i, boundaries[i].edges[boundaries[i].num_edges-1]);
					
					// Mark the first node as visited
					connections[current_ref].marked=true;
					
					// Proceed to the next node
					if (old_ref<0) 
					{
						old_ref=current_ref;
						current_ref=connections[current_ref].connecting_refs[0];
					}
					else if (connections[current_ref].connecting_refs[0]==old_ref)
					{
						old_ref=current_ref;
						current_ref=connections[current_ref].connecting_refs[1];
					}
					else if (connections[current_ref].connecting_refs[1]==old_ref)
					{
						old_ref=current_ref;
						current_ref=connections[current_ref].connecting_refs[0];
					}
					else woops(WOOPS_DATA, "Programmer screwed up");
					
				} while (connections[current_ref].marked==false);  // When a marked node is reached a loop is completed
			}
		}
				
		sfree(bnodes, HEAP_DATA);
		sfree(connections, HEAP_DATA);

		// Get the rest of the current line
		fgets(line, MAX_LINE_LENGTH, fp);
		
		// Read the end of section line
		fgets(line, MAX_LINE_LENGTH, fp);
		if (strncmp(line, "ENDOFSECTION", strlen("ENDOFSECTION"))!=0) woops(WOOPS_DATA, "End of section not found in file \"%s\"", filename);
	}

	// Set the boundary definitions of the nodes
	for (i=0; i<num_edges; i++)
	{
		if (edges[i].boundary==true)
		{
			if (nodes[edges[i].nodes[0]].boundary_edges[0]<0)
			{
				nodes[edges[i].nodes[0]].boundary_edges[0]=i;
			}
			else if (nodes[edges[i].nodes[0]].boundary_edges[1]<0)
			{
				nodes[edges[i].nodes[0]].boundary_edges[1]=i;
			}
			else woops(WOOPS_DATA, "Attempt to create more than 2 boundary references for a node");
			
			if (nodes[edges[i].nodes[1]].boundary_edges[0]<0)
			{
				nodes[edges[i].nodes[1]].boundary_edges[0]=i;
			}
			else if (nodes[edges[i].nodes[1]].boundary_edges[1]<0)
			{
				nodes[edges[i].nodes[1]].boundary_edges[1]=i;
			}
			else woops(WOOPS_DATA, "Attempt to create more than 2 boundary references for a node");
		}
	}

	// Check to make sure that every edge has either two adjacent elements or one element and one boundary edge
	for (i=0; i<num_edges; i++)
	{
		if (edges[i].element1<0 || edges[i].element1>=num_elements) woops(WOOPS_DATA, "No element1 adjacent to edge (%ld)!", i);
		if (edges[i].boundary==true)
		{
			if (edges[i].other<0 || edges[i].other>=num_boundaries) woops(WOOPS_DATA, "Boundary edge found which does not lie on any tagged boundary (edge %ld has: other=%ld, nodes = %ld and %ld). This can be caused by a bug in Gambit, where nodes are duplicated on the boundary between adjacent faces.", i, edges[i].other, edges[i].nodes[0], edges[i].nodes[1]);
		}
		else
		{
			if (edges[i].other<0 || edges[i].other>=num_elements) woops(WOOPS_DATA, "Internal edge (%ld) found without two adjacent elements. This can be caused by a bug in Gambit, where nodes are duplicated on the boundary between adjacent faces.", i);
		}
	}

	// Compute the node scales
	for (i=0; i<num_nodes; i++)
	{
		double Aproduct=1.0;
		for (ii=0; ii<nodes[i].num_elements; ii++)
		{
			long int el;

			el=nodes[i].elements[ii];

			Aproduct*=elements[el].A;
		}
		nodes[i].scale=pow(Aproduct, 0.5/((double)nodes[i].num_elements));
	}

	// Calculate the internal edge interpolation properties
	for (i=0; i<num_edges; i++)
	{
		if (edges[i].boundary==false)
		{
			// First, it is necessary to identify which of the two nodes is na and which is nb
			if ((nodes[edges[i].nodes[0]].x-edges[i].x)*edges[i].lx + (nodes[edges[i].nodes[0]].y-edges[i].y)*edges[i].ly > 0.0)
			{
				edges[i].na=edges[i].nodes[1];
				edges[i].nb=edges[i].nodes[0];
			}
			else
			{
				edges[i].na=edges[i].nodes[0];
				edges[i].nb=edges[i].nodes[1];
			}

			// Now, identify which element is e1 and which is e2
			if ((elements[edges[i].element1].x-edges[i].x)*edges[i].nx + (elements[edges[i].element1].y-edges[i].y)*edges[i].ny > 0.0)
			{
				edges[i].e1=edges[i].other;
				edges[i].e2=edges[i].element1;
			}
			else
			{
				edges[i].e1=edges[i].element1;
				edges[i].e2=edges[i].other;
			}

			// Calculate the (x,y) coordinates of the intercept
			x1=elements[edges[i].e1].x;
			y1=elements[edges[i].e1].y;
			x2=elements[edges[i].e2].x;
			y2=elements[edges[i].e2].y;
			xa=nodes[edges[i].na].x;
			ya=nodes[edges[i].na].y;
			xb=nodes[edges[i].nb].x;
			yb=nodes[edges[i].nb].y;
			
			det=(xa-xb)*(y2-y1)-(ya-yb)*(x2-x1);
			if (fabs(det)<DBL_EPSILON) woops(WOOPS_DATA, "Line connecting adjacent element centroids is mysteriously parallel to the edge");

			//qa=((y2-y1)*(x2-xb)+(x1-x2)*(y2-yb))/det;
			qn=((yb-ya)*(x2-xb)+(xa-xb)*(y2-yb))/det;

			edges[i].x_int=qn*x1+(1.0-qn)*x2;
			edges[i].y_int=qn*y1+(1.0-qn)*y2;
			
			// Calculate the coordinates of e1 and e2 in (n, l) space
			edges[i].e1n=(elements[edges[i].e1].x - edges[i].x_int)*edges[i].nx + (elements[edges[i].e1].y - edges[i].y_int)*edges[i].ny;
			edges[i].e1l=(elements[edges[i].e1].x - edges[i].x_int)*edges[i].lx + (elements[edges[i].e1].y - edges[i].y_int)*edges[i].ly;
			edges[i].e2n=(elements[edges[i].e2].x - edges[i].x_int)*edges[i].nx + (elements[edges[i].e2].y - edges[i].y_int)*edges[i].ny;
			edges[i].e2l=(elements[edges[i].e2].x - edges[i].x_int)*edges[i].lx + (elements[edges[i].e2].y - edges[i].y_int)*edges[i].ly;
			
			// Calculate the l coordinate of the midpoint of the edge
			edges[i].mid_l=(edges[i].x-edges[i].x_int)*edges[i].lx + (edges[i].y-edges[i].y_int)*edges[i].ly;
		}
	}

	// Calculate the mesh summary properties
	x_min=DBL_MAX;
	x_max=-DBL_MAX;
	y_min=DBL_MAX;
	y_max=-DBL_MAX;
	for (i=0; i<num_nodes; i++)
	{
		if (nodes[i].x > x_max) x_max=nodes[i].x;
		if (nodes[i].x < x_min) x_min=nodes[i].x;
		if (nodes[i].y > y_max) y_max=nodes[i].y;
		if (nodes[i].y < y_min) y_min=nodes[i].y;
	}

	A_total=0.0;
	A_min=DBL_MAX;
	A_max=0.0;
	for (i=0; i<num_elements; i++)
	{
		A_total+=elements[i].A;
		A_min=MIN(A_min, elements[i].A);
		A_max=MAX(A_max, elements[i].A);
	}
	
	fclose(fp);

	return;
}

// Calculate the average nearest neighbour distance for the mesh, using element centres only.
// This gives a nominal measure of the grid size of the mesh. This function can only be called after the
// mesh has been loaded. 
double average_nearest_neighbour_distance(void)
{
	long int i, j;
	double x1, y1, x2, y2;
	double dist, min_dist, sum, average;

	sum=0.0;
	for (i=0; i<num_elements; i++)
	{
		x1=elements[i].x;
		y1=elements[i].y;
		
		min_dist=DBL_MAX;
		for (j=0; j<num_elements; j++)
		{
			x2=elements[j].x;
			y2=elements[j].y;

			if (i!=j)
			{
				dist=sqrt(SQ(x2-x1)+SQ(y2-y1));
				if (dist<min_dist) min_dist=dist;
			}
		}
		sum+=min_dist;
	}
	average=sum/(double)num_elements;
	
	return average;
}

// Calculate the minimum nearest neighbour distance for the mesh, using element centres only.
// This gives a nominal measure of the grid size of the mesh. This function can only be called after the
// mesh has been loaded. 
double minimum_nearest_neighbour_distance(void)
{
	long int i, j;
	double x1, y1, x2, y2;
	double dist, global_min_dist;

	global_min_dist=DBL_MAX;
	for (i=0; i<num_elements; i++)
	{
		x1=elements[i].x;
		y1=elements[i].y;
		
		for (j=0; j<num_elements; j++)
		{
			x2=elements[j].x;
			y2=elements[j].y;

			if (i!=j)
			{
				dist=sqrt(SQ(x2-x1)+SQ(y2-y1));
				if (dist<global_min_dist) global_min_dist=dist;
			}
		}
	}
	
	return global_min_dist;
}

// Returns the number of neighbouring elements adjacent to a given element. 
long int get_num_neighbours(long int el)
{
	long int i, count, ed;

	count=0;
	for (i=0; i<elements[el].N; i++)
	{
		ed=elements[el].edges[i];
		if (edges[ed].boundary==false) count++;
	}
	
	return count;
}

// Finds the angle between the x axis and the imaginary line connecting from the centroid of element el
// to the centroid of element neighbour. 
double corner_angle(long int el, long int corner_node)
{
	double dx, dy;
	
	if (el<0 || el>=num_elements || corner_node<0 || corner_node>=num_nodes) woops(WOOPS_DATA, "Illegal element / node reference");

	dx=nodes[corner_node].x-elements[el].x;
	dy=nodes[corner_node].y-elements[el].y;
	
	return atan2(dy,dx);
}

// Returns true if the normal to edge ed points inwards, from the point of view of an observer standing inside
// element el. Returns false if this is not the case. 
// (Note that an error is thrown if edge ed and element el are not adjacent.)
int inward_normal(long int ed, long int el)
{
	if (ed<0 || ed>=num_edges) woops(WOOPS_DATA, "Illegal edge reference");
	if (el<0 || el>=num_elements) woops(WOOPS_DATA, "Illegal element reference");
	if ( !( edges[ed].element1==el || (edges[ed].boundary==false && edges[ed].other==el) ) ) woops(WOOPS_DATA, "Element is not adjacent to edge");

	if (edges[ed].nx*(elements[el].x-edges[ed].x) + edges[ed].ny*(elements[el].y-edges[ed].y) > 0.0) return true;
	else return false;
}

// Finds the intersection between two lines, one passing through (xa, ya) and (xb, yb), the other passing through
// (xe, ye) and parallel to the vector (nx, ny). Sets (xi, yi) to the intersection point, l to the signed 
// distance from (xe, ye) to the intersection point (in the direction of (nx, ny)) such that
// (xe, ye) + l(nx, ny) = (xi, yi)
// and q such that
// Zi = [(1+q)/2]Za + [(1-q)/2]Zb, 
// where Z is some quantity to be interpolated. 
// Returns false if there was an error (such as the lines being parallel), true otherwise
int find_intersection(double xa, double ya, double xb, double yb, double xe, double ye, double nx, double ny, double *xi, double *yi, double *l, double *q)
{
	double det, halfdiffX, halfdiffY, avX, avY, qa, qb;

	avX=(xb+xa)/2.0;
	avY=(yb+ya)/2.0;
	
	halfdiffX=(xb-xa)/2.0;
	halfdiffY=(yb-ya)/2.0;

	det=ny*halfdiffX-nx*halfdiffY;

	if (fabs(det)<DBL_EPSILON) return false;
	
	*q=(ny*(avX-xe)-nx*(avY-ye))/det;
	*l=(halfdiffX*(avY-ye)-halfdiffY*(avX-xe))/det;

	qa=(1.0+*q)/2.0;
	qb=(1.0-*q)/2.0;

	*xi=qa*xa+qb*xb;
	*yi=qa*ya+qb*yb;

	return true;
}


// Returns true if and only if point (x, y) is inside (or on the border of) the triangle formed by
// (x1, y1), (x2, y2) and (x3, y3). 
int inside_triangle(double x, double y, double x1, double y1, double x2, double y2, double x3, double y3)
{
	double xi, yi, l, q;

	if (find_intersection(x2, y2, x3, y3, x1, y1, x-x1, y-y1, &xi, &yi, &l, &q)==true)
	{
		if (q<-1 || q>1) return false;
		if (l<1.0) return false;
	}

	if (find_intersection(x3, y3, x1, y1, x2, y2, x-x2, y-y2, &xi, &yi, &l, &q)==true)
	{
		if (q<-1 || q>1) return false;
		if (l<1.0) return false;
	}

	if (find_intersection(x1, y1, x2, y2, x3, y3, x-x3, y-y3, &xi, &yi, &l, &q)==true)
	{
		if (q<-1 || q>1) return false;
		if (l<1.0) return false;
	}

	return true;
}

// Returns true if and only if point (x, y) is inside the diamond formed by the end nodes and adjacent elements
// of edge ed (if ed is internal) or inside the triangle formed by the end nodes and adjacent element of edge ed
// (if ed is on a boundary). 
int diamond_enclosure(double x, double y, long int ed)
{
	double x1, y1, x2, y2, x3, y3, x4, y4;
	
	if (ed<0 || ed>=num_edges) woops(WOOPS_DATA, "Invalid edge reference");
	if (x<x_min || x>x_max || y<y_min || y>y_max) woops(WOOPS_DATA, "Point is outside of the domain bounding rectangle");
	
	x1=elements[edges[ed].element1].x;
	y1=elements[edges[ed].element1].y;
	x2=nodes[edges[ed].nodes[0]].x;
	y2=nodes[edges[ed].nodes[0]].y;
	x3=nodes[edges[ed].nodes[1]].x;
	y3=nodes[edges[ed].nodes[1]].y;

	if (edges[ed].boundary==true)
	{
		return inside_triangle(x, y, x1, y1, x2, y2, x3, y3);
	}
	else
	{
		x4=elements[edges[ed].other].x;
		y4=elements[edges[ed].other].y;

		return inside_triangle(x, y, x1, y1, x2, y2, x3, y3) || inside_triangle(x, y, x4, y4, x2, y2, x3, y3) || inside_triangle(x, y, x4, y4, x1, y1, x3, y3) || inside_triangle(x, y, x4, y4, x1, y1, x2, y2);
	}
}

