#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "heap.h"
#include "message.h"
#include "macros.h"

// Data structures below are for storing a list of the data items on the heap.
// (Note: The items on the heap that are used by this module itself are NOT included.)

typedef struct HeapItemStruct HeapItem;
struct HeapItemStruct
{
	void *base_address;  	// Address at which the item is stored in memory
	long int size;		// Size of the item in bytes
	long int source;	// An index into the source_locations array, for the source line which
				// created (or last sreallocated) this item.
	
	HeapItem *next;		// Pointer to the next heap item, or NULL if last in the list
};

// This is a linked list of HeapItems, stored in order of base_address
HeapItem *first_heap_item=NULL;


typedef struct
{
	char *file;		// Name of the source file
	long int line;		// Line number in the source file
	long int multiplicity;  // Number of current heap items originating from this source location
} SourceLocation;

long int num_source_locations=0;
SourceLocation *source_locations=NULL;

// Find an item in the heap list, returning pointers to the item, as well as the items on either side of it
// in the list. If any of these do not exist, NULL pointers are returned.
void find_heap_item(void *ptr, HeapItem **previous, HeapItem **item, HeapItem **next)
{
	if (ptr==NULL) woops(WOOPS_DATA, "Search for an invalid heap item");
	
	(*previous)=NULL;
	(*next)=NULL;

	(*item)=first_heap_item;

	while ((*item)!=NULL)
	{
		(*next)=(*item)->next;
		if ((*item)->base_address == ptr) return;
		else if (ptr > (*item)->base_address) break;

		(*previous)=(*item);
		(*item)=(*next);
	}

	// Not found, so all should be NULL
	(*previous)=NULL;
	(*next)=NULL;
	(*item)=NULL;
	
	return;
}

// Looks for a heap item with address ptr. If it finds it, it is deleted from the list and true is returned.
// If it can't be found, then false is returned.
int delete_heap_item(void *ptr)
{
	HeapItem *previous, *item, *next;

	if (ptr==NULL) woops(WOOPS_DATA, "Attempt to delete an invalid heap item");
	
	find_heap_item(ptr, &previous, &item, &next);

	if (item==NULL) return false;

	// Reduce the multiplicity of the source location which created this heap item
	source_locations[item->source].multiplicity--;
	
	// Delete the heap item
	if (previous==NULL) // item is first in list
	{
		free(item);
		first_heap_item=next;
	}
	else
	{
		free(item);
		previous->next=next;
	}
		
	return true;
}

// Inserts a heap item into the list. The heap item is defined by the address of its corresponding memory block.
// This function also requires source_ref to be specified, which is an index into the source_locations array
// for the source location which generated this heap item. This allows the multiplicity to be updated.
void insert_heap_item(void *ptr, long int size, long int source_ref)
{
	HeapItem *previous, *item, *next, *new;
	
	if (ptr==NULL) woops(WOOPS_DATA, "Attempt to insert an invalid heap item");
	if (size<=0) woops(WOOPS_DATA, "Attempt to insert an invalid heap item");
	if (source_ref<0 || source_ref>=num_source_locations) woops(WOOPS_DATA, "Invalid source reference");

	// Set the item's basic information
	new=(HeapItem *)malloc(sizeof(HeapItem)*1);
	if (new==NULL) woops(WOOPS_DATA, "Out of memory");
	new->base_address=ptr;
	new->size=size;
	new->source=source_ref;

	// Update the multiplicity of the corresponding source location
	source_locations[source_ref].multiplicity++;
	
	previous=NULL;
	item=first_heap_item;
	next=NULL;
	while (item!=NULL)
	{
		next=item->next;

		if (item->base_address==ptr) woops(WOOPS_DATA, "Two blocks were allocated to the same address in memory. They were allocated at line %ld of file \"%s\" and then line %ld of file \"%s\".", source_locations[item->source].line, source_locations[item->source].file, source_locations[source_ref].line, source_locations[source_ref].file);
		else if (ptr > item->base_address)  // Item to be inserted into list in front of another item(s)
		{
			new->next=item;
			
			if (previous==NULL)  // Item inserted at start of non-empty list
			{
				first_heap_item=new;
			}
			else  // Item inserted in between two heap items
			{
				previous->next=new;
			}
			
			return;
		}

		previous=item;
		item=next;
	}

	// Item to be inserted at end of a (possibly empty list)
	new->next=NULL;
	
	if (previous==NULL) // Item inserted in an empty list
	{
		first_heap_item=new;
	}
	else // Item inserted at end of a non-empty list
	{
		previous->next=new;
	}
	
	return;
}

// A safe replacement for calloc()
void *scalloc(size_t nobj, size_t size, char file[], long int line)
{
	void *p;
	long int i;
	long int source_ref;

	if (nobj<0 || size<1) woops(file, line, __DATE__, __TIME__, "Illegal call to scalloc()");

	p=calloc(nobj, size);

	if (p==NULL && nobj>0) woops(file, line, __DATE__, __TIME__, "Out of memory");

	#ifdef HEAP_DEBUG_MODE_ACTIVE
	
	// First, add the source location to the array (if necessary) and obtain an index to it.
	source_ref=-1;
	for (i=0; i<num_source_locations; i++)
	{
		if (source_locations[i].line==line && strcmp(file, source_locations[i].file)==0)
		{
			source_ref=i;
			break;
		}
	}
	if (source_ref<0 || source_ref>=num_source_locations)
	{
		num_source_locations++;
		source_locations=(SourceLocation *)realloc((void *)source_locations, sizeof(SourceLocation)*num_source_locations);
		if (source_locations==NULL) woops(WOOPS_DATA, "Out of memory");
		
		source_ref=num_source_locations-1;
		
		source_locations[source_ref].line=line;
		source_locations[source_ref].file=(char *)malloc(sizeof(char)*(strlen(file)+1));
		if (source_locations[source_ref].file==NULL) woops(WOOPS_DATA, "Out of memory");
		strcpy(source_locations[source_ref].file, file);
		source_locations[source_ref].multiplicity=0;
	}
	
	// Store the pointer for future reference.
	if (p!=NULL) insert_heap_item(p, nobj*size, source_ref);
	
	#endif
	
	return p;
}

// A safe replacement for malloc()
void *smalloc(size_t size, char file[], long int line)
{
	void *p;
	long int i;
	long int source_ref;

	if (size<0) woops(file, line, __DATE__, __TIME__, "Illegal call to smalloc()");

	p=malloc(size);

	if (p==NULL && size>0) woops(file, line, __DATE__, __TIME__, "Out of memory");

	#ifdef HEAP_DEBUG_MODE_ACTIVE

	// First, add the source location to the array (if necessary) and obtain an index to it.
	source_ref=-1;
	for (i=0; i<num_source_locations; i++)
	{
		if (source_locations[i].line==line && strcmp(file, source_locations[i].file)==0)
		{
			source_ref=i;
			break;
		}
	}
	if (source_ref<0 || source_ref>=num_source_locations)
	{
		num_source_locations++;
		source_locations=(SourceLocation *)realloc((void *)source_locations, sizeof(SourceLocation)*num_source_locations);
		if (source_locations==NULL) woops(WOOPS_DATA, "Out of memory");
		
		source_ref=num_source_locations-1;
		
		source_locations[source_ref].line=line;
		source_locations[source_ref].file=(char *)malloc(sizeof(char)*(strlen(file)+1));
		if (source_locations[source_ref].file==NULL) woops(WOOPS_DATA, "Out of memory");
		strcpy(source_locations[source_ref].file, file);
		source_locations[source_ref].multiplicity=0;
	}

	// Store this pointer for future reference
	if (p!=NULL) insert_heap_item(p, size, source_ref);
	
	#endif
	
	return p;
}

// A safe replacement for realloc()
void *srealloc(void *p, size_t size, char file[], long int line)
{
	void *ptr;
	long int i;
	long int source_ref;
	
	if (size<0) woops(file, line, __DATE__, __TIME__, "Illegal call to srealloc()");

	ptr=realloc(p, size);

	if (ptr==NULL && size>0) woops(file, line, __DATE__, __TIME__, "Out of memory");

	#ifdef HEAP_DEBUG_MODE_ACTIVE

	// First, add the source location to the array (if necessary) and obtain an index to it.
	source_ref=-1;
	for (i=0; i<num_source_locations; i++)
	{
		if (source_locations[i].line==line && strcmp(file, source_locations[i].file)==0)
		{
			source_ref=i;
			break;
		}
	}
	if (source_ref<0 || source_ref>=num_source_locations)
	{
		num_source_locations++;
		source_locations=(SourceLocation *)realloc((void *)source_locations, sizeof(SourceLocation)*num_source_locations);
		if (source_locations==NULL) woops(WOOPS_DATA, "Out of memory");
		
		source_ref=num_source_locations-1;
		
		source_locations[source_ref].line=line;
		source_locations[source_ref].file=(char *)malloc(sizeof(char)*(strlen(file)+1));
		if (source_locations[source_ref].file==NULL) woops(WOOPS_DATA, "Out of memory");
		strcpy(source_locations[source_ref].file, file);
		source_locations[source_ref].multiplicity=0;
	}

	// Store this pointer for future reference
	if (p!=NULL) 
	{
		if (delete_heap_item(p)==false) woops(WOOPS_DATA, "Attempt to reallocate an invalid pointer on line %ld of file \"%s\"", line, file);
	}
	if (ptr!=NULL) insert_heap_item(ptr, size, source_ref);
	
	
	#endif
	
	return ptr;
}

// A safe replacement for free()
void sfree(void *p, char file[], long int line)
{
	if (p==NULL) return;

	#ifdef HEAP_DEBUG_MODE_ACTIVE

	// Check to make sure that a valid pointer is known at this address, and throw error if not.
	// Delete the heap item if found.
	if (delete_heap_item(p)==false) woops(WOOPS_DATA, "Attempt to free an invalid pointer on line %ld of file \"%s\"", line, file);	

	#endif
	
	free(p);
}

// Display the current contents of the heap. Output is written to the specified stream.
// (This is useful to detect memory leaks.)
void display_heap(FILE *fp)
{
	long int i;
	HeapItem *item;
	long int size_total;
	
	if (fp==NULL) woops(WOOPS_DATA, "Invalid file or stream");

	for (i=0; i<num_source_locations; i++)
	{
		if (source_locations[i].multiplicity<0) woops(WOOPS_DATA, "Multiplicity error");
		else if (source_locations[i].multiplicity>0)
		{
			fprintf(fp, "File \"%s\" line %ld: %ld allocated blocks\n", source_locations[i].file, source_locations[i].line, source_locations[i].multiplicity);
		}
	}
	fprintf(fp, "\n");
	
	size_total=0;
	item=first_heap_item;
	while(item!=NULL)
	{
		fprintf(fp, "Address: %lu, Size: %ld, Origin: %s line %ld\n", item->base_address, item->size, source_locations[item->source].file, source_locations[item->source].line);
		size_total+=item->size;
	
		item=item->next;
	}

	fprintf(fp, "\nTotal Memory in Heap = %ld bytes\n", size_total);

	return;
}

// Call this function just before exiting. It will throw errors if there are any items remaining on the heap
// (apart from the ones used by this module itself). This function also cleans up after its own usage of the heap.
void close_heap(void)
{
	long int i;
	HeapItem *item, *next;
	
	// Report any memory leaks
	
	printf("\nThe following memory leaks exist:\n");
	display_heap(stdout);
	
	// Destroy the source_locations array
	for (i=0; i<num_source_locations; i++)
	{
		if (source_locations[i].file!=NULL) free(source_locations[i].file);
	}
	if (source_locations!=NULL) free(source_locations);
	source_locations=NULL;
	num_source_locations=0;

	// Destroy the heap items list
	item=first_heap_item;
	while (item!=NULL)
	{
		next=item->next;
		free(item);
		item=next;
	}

	return;
}

