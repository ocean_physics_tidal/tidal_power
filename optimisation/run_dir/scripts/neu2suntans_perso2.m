% NEU2SUNTANS  Convert Gambit .neu file to suntans readable grid.
%   NEU2SUNTANS(MESH_FILE,DATADIR) reads the .neu file specified
%   by MESH_FILE and creates the points.dat, edges.dat, cells.dat files
%   that are used by SUNTANS in the directory specified by DATADIR.
%   If only one argument is specified then it is assumed that DATADIR is
%   the local directory.  If no arguments are specified, then the
%   default neu file is assumed to be default.neu

% Sheng Chen
% ICME, Stanford University
% Febrary 02, 2006
% Modify by Alice Harang, Otago University
%function neu2suntans_perso%(varargin)
addpath('~/mfiles')


mesh_file= 'fluent-ttf-constr-Gauss.neu';
    datadir='.';

  if(~exist(mesh_file,'file'))
    error(sprintf('File %s does not exist',mesh_file));
  end
  if(~exist(datadir,'dir'))
    error(sprintf('Directory %s does not exist',datadir));
  end
  points_file = [datadir,'/points.dat'];
  edges_file = [datadir,'/edges.dat'];
  cells_file = [datadir,'/cells.dat'];

  % Start the counter
  tic;

  % read data from Gambit Neutral file
  % you can change the input file name if needed
  disp(sprintf('Processing %s ...', mesh_file))
  meshf = fopen(mesh_file,'r');
  
  for ii=1:6
    fgetl(meshf);
  end  
  num = fscanf(meshf,'%d'); np = num(1); nc = num(2); ng=num(3); nbc=num(4);
  
  if nbc~=2
      itype=0; %def the kind of implementation of BC
  else
      itype=1;
  end
  
  %read points
  info = fscanf(meshf,'%s',4);
  point = fscanf(meshf,'%d %e %e',[3, np]); point = point';
  
  %read cells
  info = fscanf(meshf,'%s',3);
  if itype==1
    cell = fscanf(meshf,'%d %d %d %d %d %d',[6, nc]); cell = cell';
  else 
    cell = fscanf(meshf,'%d %d %d %d %d %d %d',[7, nc]); cell = cell';
  end
  
  if ~isempty(find(cell(:,2)~=3)>0)
      itype=2; %quad grids or hybrides
  end
  
  % sort the point number such that in the adjacency matrix
  % we only need to care about the upper triangular part
  cell_all=cell;
  cell=zeros(size(cell_all,1),4);
  for i=1:size(cell_all,1)
      if cell_all(i,2)==3
          cell(i,1:3) = sort(cell_all(i, 4:6), 2);
      else
          cell(i,1:4) = sort(cell_all(i,4:7),2);
      end
  end
  
  %Pass groups
  for ii=1:ng
    info = fscanf(meshf,'%s',13);
    group = fscanf(meshf,'%d');
  end
  
  %Read Boundaries
  if itype == 1 %old version
    info = fscanf(meshf,'%s',6);
    num1 = fscanf(meshf,'%d',4);
    if(itype~=num(1))
        error(sprintf('Error between BC implementation and number of BC'));
    end
    if(isempty(num1))
      nBC1 = 0;
      BC1 = [];
    else
      nBC1 = num1(2);
      BC1 = fscanf(meshf,'%d %d %d',[3, nBC1]); BC1 = BC1';
    end
  
    info = fscanf(meshf,'%s',6);
    num2 = fscanf(meshf,'%d',4);
    if(isempty(num2))
      nBC2 = 0;
      BC2 = [];
    else
      nBC2 = num2(2);
      BC2 = fscanf(meshf,'%d %d %d',[3, nBC2]); BC2 = BC2';
    end
  else % (itype==0 || itype==2) %recent tri or quad
    clear num nBC BC BCname
    for n=1:nbc
      info = fscanf(meshf,'%s',4);
      BCname = fscanf(meshf,'%s',1);
      if strfind(BCname,'TIDE')==1
        BCtype(n)=3;
      elseif strfind(BCname,'WALL')==1
        if (ng>1 && isempty(strfind(BCname,'WALL_S')) && isempty(strfind(BCname,'WALL_N')))
          BCtype(n)=0;
        else
          BCtype(n)=1;
        end
      else
        BCtype(n)=0;
      end
      num(n,:) = fscanf(meshf,'%d',4);
      %if(itype~=num(n,1))
      %  error(sprintf('Error between BC implementation and number of BC'));
      %end
      nBC(n) = num(n,2);
      if n>1
        TEMP=NaN*ones(n,max(num(:,2)));
        TEMP(1:n-1,1:max(num(1:n-1,2)))=BC(:,1:max(num(1:n-1,2)));
        TEMP(n,1:nBC(n))=fscanf(meshf,'%d',[1, nBC(n)]);
        BC=TEMP;
      else
        BC(n,:) = fscanf(meshf,'%d',[1, nBC(n)]);
      end
    end
    BC=BC';
  end
  
  status = fclose(meshf);
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % convert Gambit to Suntans
  
  % x0 and y0 are the values to be subtracted in order to keep the origin at (0,0)
  x = point(:, 2); y = point(:, 3);
  points = [x, y, zeros(np, 1)];
  
  % adjacency matrix
  A = sparse(np, np);
  Vp1 = sparse(np, np); Vp2 = sparse(np, np);
  xv = zeros(nc, 1); yv = zeros(nc, 1);

  for i = 1:nc
      % calculate the circumcenter of each cell, which is the Voronoi point in
      % the triangulation
      if (itype==2 && cell_all(i,2)==2)
          p1 = cell(i,1); p2 = cell(i,2); p3 = cell(i,3); p4=cell(i,4);
          %reorganisation of points in clockwise direction
          xcenter=mean([x(p1),x(p2),x(p3), x(p4)]); ycenter=mean([y(p1),y(p2),y(p3), y(p4)]);
          theta(1)=2*atan((y(p1)-ycenter)/((x(p1)-xcenter)+sqrt((x(p1)-xcenter)^2+(y(p1)-ycenter)^2)));
          theta(2)=2*atan((y(p2)-ycenter)/((x(p2)-xcenter)+sqrt((x(p2)-xcenter)^2+(y(p2)-ycenter)^2)));
          theta(3)=2*atan((y(p3)-ycenter)/((x(p3)-xcenter)+sqrt((x(p3)-xcenter)^2+(y(p3)-ycenter)^2)));
          theta(4)=2*atan((y(p4)-ycenter)/((x(p4)-xcenter)+sqrt((x(p4)-xcenter)^2+(y(p4)-ycenter)^2)));
          polypoints=sortrows([theta',[x(p1),x(p2),x(p3), x(p4)]', [y(p1),y(p2),y(p3), y(p4)]', [p1,p2,p3,p4]'],1);
          [geom, iner, cpmo] = polygeom(polypoints(:,2),polypoints(:,3));
          %plot([x(p1),x(p2),x(p3), x(p4)], [y(p1),y(p2),y(p3), y(p4)],'*')
          %hold on
          %plot(geom(2), geom(3), 'r*')
          xv(i)=geom(2); yv(i)=geom(3); 
          %reorder of point in clockwise direction
          p1=polypoints(1,4); p2=polypoints(2,4); p3=polypoints(3,4); p4=polypoints(4,4); 
          cell(i,1)=p1;cell(i,2)=p2;cell(i,3)=p3;cell(i,4)=p4;
          
          if p1<p2 %to conserve the focus on the upper triangular part in the adjacency matrix
              if A(p1, p2) == 0  Vp1(p1, p2) = i; else Vp2(p1, p2) = i; end %if
              A(p1, p2) = A(p1, p2) + 1;
          else
              if A(p2, p1) == 0  Vp1(p2, p1) = i; else Vp2(p2, p1) = i; end %if
              A(p2, p1) = A(p2, p1) + 1;
          end
          
          if p2<p3
              if A(p2, p3) == 0  Vp1(p2, p3) = i; else Vp2(p2, p3) = i; end %if
              A(p2, p3) = A(p2, p3) + 1;
          else
              if A(p3, p2) == 0  Vp1(p3, p2) = i; else Vp2(p3, p2) = i; end %if
              A(p3, p2) = A(p3, p2) + 1;
          end
          
          if p3<p4
              if A(p3, p4) == 0  Vp1(p3, p4) = i; else Vp2(p3, p4) = i; end %if
              A(p3, p4) = A(p3, p4) + 1;
          else
              if A(p4, p3) == 0  Vp1(p4, p3) = i; else Vp2(p4, p3) = i; end %if
              A(p4, p3) = A(p4, p3) + 1;
          end
          
          if p1<p4
              if A(p1, p4) == 0  Vp1(p1, p4) = i; else Vp2(p1, p4) = i; end %if
              A(p1, p4) = A(p1, p4) + 1;
          else
              if A(p4, p1) == 0  Vp1(p4, p1) = i; else Vp2(p4, p1) = i; end %if
              A(p4, p1) = A(p4, p1) + 1;
          end
          
      else
          p1 = cell(i,1); p2 = cell(i,2); p3 = cell(i,3);
          [xv(i), yv(i)] = circumcenter(x(p1), y(p1), x(p2), y(p2), x(p3), y(p3));
          
          if A(p1, p2) == 0
              Vp1(p1, p2) = i;
          else
              Vp2(p1, p2) = i;
          end %if
          
          if A(p1, p3) == 0
              Vp1(p1, p3) = i;
          else
              Vp2(p1, p3) = i;
          end %if
          
          if A(p2, p3) == 0
              Vp1(p2, p3) = i;
          else
              Vp2(p2, p3) = i;
          end %if
          
          A(p1, p2) = A(p1, p2) + 1; A(p1, p3) = A(p1, p3) + 1; A(p2, p3) = A(p2, p3) + 1;
      end
  end
  
  [I,J] = find(A>0);
  ne = length(I);
  % edge = full([I-1, J-1, A((J-1)*np+I)~=2, Vp1((J-1)*np+I)-1, Vp2((J-1)*np+I)-1]);
  % using the following way to avoiding the index over the integer limit
  edge = zeros(ne, 5);
  for k = 1:ne
    i = I(k); j = J(k); edge(k, :) = [i-1, j-1, A(i,j)~=2, Vp1(i,j)-1, Vp2(i,j)-1];
  end %k
  
  % apply BC
  if itype == 1
  % apply offshore BC and set the corresponding type to 2
    for i = 1:nBC2
      for j = 1:ne
        if edge(j, 5) == -1 & edge(j, 4) == BC2(i,1) - 1
          edge(j, 3) = 2;
        end    
      end
    end   
  elseif ng==2
    for n=1:nbc
      if BCtype(n)~=1
        for e = 1:ne
          for i = 1:nBC(n)
            for j = 1:nBC(n)
              if I(e) == BC(i,n) & J(e) == BC(j,n)
                edge(e, 3) = BCtype(n);
              end
            end
          end
        end     
      end
    end    
  else %itype=0
    for n=1:nbc
      if BCtype(n)==3
        for e = 1:ne
          for i = 1:nBC(n)
            for j = 1:nBC(n)
              if I(e) == BC(i,n) & J(e) == BC(j,n)
                edge(e, 3) = 3;
              end
            end
          end
        end    
      end
    end    
  end
  
  % obtain the neighbor information of each cell
  
  neighbor = zeros(nc,4);
  for i = 1:nc
      if (itype==2 && cell_all(i,2)==2)
          p1 = cell(i,1); p2 = cell(i,2); p3 = cell(i,3); p4 = cell(i,4);
          xcenter=mean([x(p1),x(p2),x(p3), x(p4)]); ycenter=mean([y(p1),y(p2),y(p3), y(p4)]);
          theta(1)=2*atan((y(p1)-ycenter)/((x(p1)-xcenter)+sqrt((x(p1)-xcenter)^2+(y(p1)-ycenter)^2)));
          theta(2)=2*atan((y(p2)-ycenter)/((x(p2)-xcenter)+sqrt((x(p2)-xcenter)^2+(y(p2)-ycenter)^2)));
          theta(3)=2*atan((y(p3)-ycenter)/((x(p3)-xcenter)+sqrt((x(p3)-xcenter)^2+(y(p3)-ycenter)^2)));
          theta(4)=2*atan((y(p4)-ycenter)/((x(p4)-xcenter)+sqrt((x(p4)-xcenter)^2+(y(p4)-ycenter)^2)));
          polypoints=sortrows([theta',[x(p1),x(p2),x(p3), x(p4)]', [y(p1),y(p2),y(p3), y(p4)]', [p1,p2,p3,p4]'],1);
          p1=polypoints(1,4); p2=polypoints(2,4); p3=polypoints(3,4); p4=polypoints(4,4); 
         
          if p1<p2 
              if Vp1(p1,p2) == i neighbor(i, 1) = Vp2(p1,p2); else neighbor(i, 1) = Vp1(p1,p2); end
          else
              if Vp1(p2,p1) == i neighbor(i, 1) = Vp2(p2,p1); else neighbor(i, 1) = Vp1(p2,p1); end
          end
          
          if p2<p3 
              if Vp1(p2,p3) == i neighbor(i, 2) = Vp2(p2,p3); else neighbor(i, 2) = Vp1(p2,p3); end
          else
              if Vp1(p3,p2) == i neighbor(i, 2) = Vp2(p3,p2); else neighbor(i, 2) = Vp1(p3,p2); end
          end
                    
          if p3<p4 
              if Vp1(p3,p4) == i neighbor(i, 3) = Vp2(p3,p4); else neighbor(i, 3) = Vp1(p3,p4); end
          else
              if Vp1(p4,p3) == i neighbor(i, 3) = Vp2(p4,p3); else neighbor(i, 3) = Vp1(p4,p3); end
          end
                    
          if p1<p4 
              if Vp1(p1,p4) == i neighbor(i, 4) = Vp2(p1,p4); else neighbor(i, 4) = Vp1(p1,p4); end
          else
              if Vp1(p4,p1) == i neighbor(i, 4) = Vp2(p4,p1); else neighbor(i, 4) = Vp1(p4,p1); end
          end
      
      else
          p1 = cell(i,1); p2 = cell(i,2); p3 = cell(i,3);
          if Vp1(p1,p2) == i
              neighbor(i, 1) = Vp2(p1,p2);
          else
              neighbor(i, 1) = Vp1(p1,p2);
          end
          
          if Vp1(p1,p3) == i
              neighbor(i, 2) = Vp2(p1,p3);
          else
              neighbor(i, 2) = Vp1(p1,p3);
          end
          
          if Vp1(p2,p3) == i
              neighbor(i, 3) = Vp2(p2,p3);
          else
              neighbor(i, 3) = Vp1(p2,p3);
          end
      end
  end
  
  % write data to Suntans required files
  pointsf = fopen(points_file, 'w');
  fprintf(pointsf, '%12.10e %12.10e %d\n', points');
  status = fclose(pointsf);
  
  edgesf = fopen(edges_file, 'w');
  fprintf(edgesf, '%1.0f %1.0f %1.0f %1.0f %1.0f\n', edge');
  status = fclose(edgesf);
  
  cellsf = fopen(cells_file,'w');
  for i=1:size(cell,1)
      if cell_all(i,2)==3
          cells = [3, xv(i), yv(i), cell(i,1:3)-1, neighbor(i,1:3)-1];
          fprintf(cellsf, '%1.0f %12.10e %12.10e %1.0f %1.0f %1.0f %1.0f %1.0f %1.0f\n', cells');

      else
          cells = [4 xv(i), yv(i), cell(i,1:4)-1, neighbor(i,1:4)-1];
          fprintf(cellsf, '%1.0f %12.10e %12.10e %1.0f %1.0f %1.0f %1.0f %1.0f %1.0f %1.0f %1.0f\n', cells');
      end
  end
  status = fclose(cellsf);        
  
  fprintf('Execution time = %.3f min (%.1f sec)\n',toc/60,toc);
  
