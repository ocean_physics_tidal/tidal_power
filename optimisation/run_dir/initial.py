#! /usr/bin/env python3.4
# Functions for initial setup and for launching simulations

import os
import shutil
from init_cycle_without_turb_v2 import init_cycle_without_turb_v2
from launch_power_curve_v2 import launch_power_curve_v2
import scipy.optimize
import sys
import fileops

# Changes turbine parameters based on the minimisation variable and the run_number
# Then does the book-keeping for changing 
def modify_turbines(minvariable, run_number, option):
  r3 = minvariable
  eps = 0.3 + run_number / 10.0
  X_f = [0.0, 0.0]
  Y1_f = [-250.0, 150.0]
  Y2_f = [-150.0, 250.0]

  fileops.turbinebookkeeping(r3, eps, X_f, Y1_f, Y2_f, option)

# Initial setup. Checks for command line argument, sets directories and recompiles sources on initial run
def setup(option):

  if(len(sys.argv) == 1):
    print("Command line parameter missing. USAGE: 'python Parameters-and-Launch.py n', n = 0 for initial")
    exit()
  else:
    option['run_number'] = int(sys.argv[1])

  # Checks whether running on a SLURM node. 
  # This controls whether to use mpirun or srun and to use the scratch drive 
  if option['slurmnode'] == 1:
    option['runwith'] = 'srun' # 'srun' if using on SLURM node, 'mpirun' otherwise
    option['dir_scratch'] = os.environ['SCRATCH_DIR']
  else:
    option['runwith'] = 'mpirun'
    option['dir_scratch'] = option['dir_run']     # Use the run directory as scratch if there is none

  if(option['run_number'] == 0): 
    newdir = option['dir_run'] + '/' + option['dir_simu']
    if os.path.exists(newdir):
      shutil.rmtree(newdir)
    os.makedirs(newdir)
    fileops.recompile(option['dir_run'], option['dir_sources'], option['dir_sources_ext'])
    fileops.saveoptions(option, newdir + '/parameters.dat')

  option['iterno'] = 0

# This is a function that runs before running simulation with turbines
# Needed because the optimisation requires a variable to be returned
# Returns the negative of the power because optimisation function is trying to minimise
def intermediate(minvariable, option, mesh, procs, phi, amp):
  modify_turbines(minvariable, option['run_number'], option) 
  folder = fileops.foldername(mesh, procs, phi, amp)
  fileops.saveoptions(option, option['dir_scratch'] + '/' + option['dir_simu'] + '/parameters' + str(option['run_number']) + '.dat')

  power = launch_power_curve_v2(folder, procs, option)    # Run the simulation
  option['iterno'] = option['iterno'] + 1
  return -power  

# Launch all the simulations
def launch_all(option):

  for mesh in option['mesh_used']:
    for procs in option['num_procs']:
      for phii in option['phi']:
        for amp in option['amplitude']:
          if(option['run_number'] == 0):  # initial simulation
            init_cycle_without_turb_v2(mesh, procs, phii, amp, option)
          else:
            if(option['optimise'] == 1):
              option['iterno'] = 0    # For recording which iteration is currently running
              print(scipy.optimize.minimize_scalar(intermediate, method='bounded', 
                bounds=[option['minvalue'], option['maxvalue']], 
                args=(option, mesh, procs, phii, amp, ), options={'xatol': option['abstolerance']},
                tol=option['abstolerance']))
            else:
              r3 = 0.5  # This could be placed better
              print(intermediate(r3, option, mesh, procs, phii, amp))
