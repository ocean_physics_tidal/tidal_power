#define HEAP_DATA __FILE__, __LINE__
extern void *scalloc(size_t nobj, size_t size, char file[], long int line);
extern void *smalloc(size_t size, char file[], long int line);
extern void *srealloc(void *p, size_t size, char file[], long int line);
extern void sfree(void *p, char file[], long int line);
extern void display_heap(FILE *fp);
extern void close_heap(void);

// Comment out the line below if you wish to turn off heap checking
// (do this only once all debugging is complete, as it improves efficiency
// but is no safer than the usual C library functions for memory management)
//#define HEAP_DEBUG_MODE_ACTIVE

