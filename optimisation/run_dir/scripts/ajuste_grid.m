%% Erreur dans le fichier point : des points sont en doublent (from test_mesh)

clear all
close all

%Jonction of the files from struct and unstruct mesh: edges1/2, cells1/2 et points1/2.

%junction of points file
points=load('points1.dat');
L_points1=size(points,1);
points=[points;load('points2.dat')];

%junction of edge file
edges=load('edges1.dat');
L_edges1=size(edges,1);
edges_suppl=load('edges2.dat');
edges_suppl(:,1)=edges_suppl(:,1)+L_points1;
edges_suppl(:,2)=edges_suppl(:,2)+L_points1;
%correction of voronoi points 4/5 colonnes of edges
cells=load('cells1.dat');
L_cells1=size(cells,1);
for i=1:size(edges_suppl,1)
    if edges_suppl(i,4)~=-1
        edges_suppl(i,4)=edges_suppl(i,4)+L_cells1;
    end
    if edges_suppl(i,5)~=-1
        edges_suppl(i,5)=edges_suppl(i,5)+L_cells1;
    end
end
edges=[edges;edges_suppl];

%junction of cell file
cells=load('cells1.dat');
L_cells1=size(cells,1);
cells_suppl=load('cells2.dat');
for i=1:size(cells_suppl,1)
    if cells_suppl(i,1)==3
        cells_suppl(i,4)=cells_suppl(i,4)+L_points1;
        cells_suppl(i,5)=cells_suppl(i,5)+L_points1;
        cells_suppl(i,6)=cells_suppl(i,6)+L_points1;
        if cells_suppl(i,7)~=-1
            cells_suppl(i,7)=cells_suppl(i,7)+L_cells1;
        end
        if cells_suppl(i,8)~=-1
            cells_suppl(i,8)=cells_suppl(i,8)+L_cells1;
        end
        if cells_suppl(i,9)~=-1
            cells_suppl(i,9)=cells_suppl(i,9)+L_cells1;
        end
        cells_suppl(i,10)=NaN;
        cells_suppl(i,11)=NaN;
    elseif cells_suppl(i,1)==4
        cells_suppl(i,4)=cells_suppl(i,4)+L_points1;
        cells_suppl(i,5)=cells_suppl(i,5)+L_points1;
        cells_suppl(i,6)=cells_suppl(i,6)+L_points1;
        cells_suppl(i,7)=cells_suppl(i,7)+L_points1;
        if cells_suppl(i,8)~=-1
            cells_suppl(i,8)=cells_suppl(i,8)+L_cells1;
        end
        if cells_suppl(i,9)~=-1
            cells_suppl(i,9)=cells_suppl(i,9)+L_cells1;
        end
        if cells_suppl(i,10)~=-1
            cells_suppl(i,10)=cells_suppl(i,10)+L_cells1;
        end
        if cells_suppl(i,11)~=-1
            cells_suppl(i,11)=cells_suppl(i,11)+L_cells1;
        end
    end
end
cells_tot=NaN*ones(size(cells,1)+size(cells_suppl,1),11);
for i=1:size(cells,1)
    cells_tot(i,1:length(cells(i,:)))=cells(i,:);
end
for i=1:size(cells_suppl,1)
    cells_tot(i+L_cells1,1:length(cells_suppl(i,:)))=cells_suppl(i,:);
end

cells=cells_tot;


%save to files
fid = fopen('edges.dat_before', 'w');
for i=1:size(edges,1)
    for j=1:size(edges,2)
        fprintf(fid,'%d ',edges(i,j));
    end
    fprintf(fid,'\n');
end
fclose(fid)

fid = fopen('points.dat_before', 'w');
for i=1:size(points,1)
    for j=1:size(points,2)
        fprintf(fid,'%d ',points(i,j));
    end
    fprintf(fid,'\n');
end
fclose(fid)

fid = fopen('cells.dat_before', 'w');
for i=1:size(cells,1)
    for j=1:length(cells(i,:))
        if ~isnan(cells(i,j))
            fprintf(fid,'%d ',cells(i,j));
        end
    end
    fprintf(fid,'\n');
end
fclose(fid)


%%Correction of the files

% copyfile('points.dat','points.dat_before','f')
% copyfile('edges.dat','edges.dat_before','f')
% copyfile('cells.dat','cells.dat_before','f')
% 
clear all
close all


points=load('points.dat_before');
erreur=[];
for i=1:size(points,1)
    for j=i:size(points,1)
        if i~=j
            if(((points(i,1)-points(j,1)).^2+(points(i,2)-points(j,2)).^2)<3^2)
                %find(points(i,1)==points(j,1)&&points(i,2)==points(j,2))==1)
                erreur=[erreur;[points(i,1),points(i,2)]];
            %erreur=[erreur,find(points(i,1)==points(j,1)&&points(i,2)==points(j,2)) ];
            end
        end
    end
end                

edges=load('edges.dat_before');

filename = 'cells.dat_before';
delimiter = ' ';
formatSpec = '%f%f%f%f%f%f%f%f%f%f%f%[^\n\r]';
fileID = fopen(filename,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'MultipleDelimsAsOne', true, 'EmptyValue' ,NaN, 'ReturnOnError', false);
fclose(fileID);
cells = [dataArray{1:end-1}];
clearvars filename delimiter formatSpec fileID dataArray ans;

%%%%%%%%%%%%%FINIR DE LE MODIFIER%%%%%%%%%%%%%%%%%%%%%%%%
%Correction for the edges constituated 0-segment
%edges=edges(edges(:,1)~=edges(:,2),:);



if ~isempty(erreur)
    
    points2=NaN*ones(size(points,1)-size(erreur,1), size(points,2));
    I=1;
    Modif=0;
    List_modif=[];% Point supprime, point remplacant, nouvell coordonnee du point remplacant
    List_points=[];%Old ref points, new ref points
    for i=1:size(points,1)
        for j=i:size(points,1)
            if i<j
                if(((points(i,1)-points(j,1)).^2+(points(i,2)-points(j,2)).^2)<3^2)
                    Modif=1;
                    List_modif=[List_modif;[i,j,j]];
                end
            end
        end
        if Modif==0
            points2(I,:)=points(i,:);
            List_points=[List_points;[i,I]];
            I=I+1;
        else
            Modif=0;
        end
    end
    for ii=1:size(List_modif,1)
        for jj=1:size(List_modif,1)
            if List_modif(jj,3)>List_modif(ii,1)
                List_modif(ii,3)=List_modif(ii,3)-1;
            end
        end
    end
    
    %edges modif
    for ii=1:size(edges,1)
        M=[0,0];
        %Modification on points suppressed
        for i=1:size(List_modif,1)
            if edges(ii,1)==List_modif(i,1)-1
                edges(ii,1)=List_modif(i,3)-1;
                M(1)=1;
            elseif edges(ii,2)==List_modif(i,1)-1
                edges(ii,2)=List_modif(i,3)-1;
                M(2)=1;
            end
        end
        %Modification to adjust indice of all points because of points suppressed
        if M(1)==0
            N=find(edges(ii,1)==List_points(:,1)-1);
            edges(ii,1)=List_points(N,2)-1;
        end
        if M(2)==0
            N=find(edges(ii,2)==List_points(:,1)-1);
            edges(ii,2)=List_points(N,2)-1;
        end
    end
    %cell modif
    for ii=1:size(cells,1)
        M=[0,0,0,0];
        for i=1:size(List_modif,1)
            if cells(ii,4)==List_modif(i,1)-1
                cells(ii,4)=List_modif(i,3)-1;
                M(1)=1;
            elseif cells(ii,5)==List_modif(i,1)-1
                cells(ii,5)=List_modif(i,3)-1;
                M(2)=1;
            elseif cells(ii,6)==List_modif(i,1)-1
                cells(ii,6)=List_modif(i,3)-1;
                M(3)=1;
            elseif (cells(ii,1)==4 && cells(ii,7)==List_modif(i,1)-1)
                cells(ii,7)=List_modif(i,3)-1;
                M(4)=1;
            end
        end
        if M(1)==0
            N=find(cells(ii,4)==List_points(:,1)-1);
            cells(ii,4)=List_points(N,2)-1;
        end
        if M(2)==0
            N=find(cells(ii,5)==List_points(:,1)-1);
            cells(ii,5)=List_points(N,2)-1;
        end
        if M(3)==0
            N=find(cells(ii,6)==List_points(:,1)-1);
            cells(ii,6)=List_points(N,2)-1;
        end
        if (cells(ii,1)==4 && M(4)==0)
            N=find(cells(ii,7)==List_points(:,1)-1);
            cells(ii,7)=List_points(N,2)-1;
        end
    end 
end



%check if edges file is ok:
erreur2=[];
for i=1:size(edges,1)
    for j=i:size(edges,1)
        if i~=j
            erreur2=[erreur2,find((edges(i,1)==edges(j,1)&&edges(i,2)==edges(j,2))...
                ||(edges(i,2)==edges(j,1)&&edges(i,1)==edges(j,2))) ];
        end
    end
end


if ~isempty(erreur2)
    %Correction if edges of the internal boundary in double
    edges2=NaN*ones(size(edges,1)-size(erreur2,2), size(edges,2));
    Modif=0;
    I=1;
    List_modif=[];% edge deleted (location in edge file), edge conserved (location in edge file),
                  % points associated to this edge (point1,point2))
    for i=1:size(edges,1)
        for j=i:size(edges,1)
            if i<j
                if(find((edges(i,1)==edges(j,1)&&edges(i,2)==edges(j,2))...
                        ||(edges(i,2)==edges(j,1)&&edges(i,1)==edges(j,2)))==1)
                    Modif=1;
                    List_modif=[List_modif;[i,j,edges(i,1),edges(i,2)]];
                    edges(j,3)=0;
                    if edges(j,4)==-1
                        edges(j,4)=max(edges(i,4), edges(i,5));
                    elseif edges(j,5)==-1
                        edges(j,5)=max(edges(i,4), edges(i,5));
                    end
                end
            end
        end
        if Modif==0
            edges2(I,:)=edges(i,:);
            I=I+1;
        else
            Modif=0;
        end
    end
    
    %Correction of the cells files to modify neighbour cells
    for i=1:size(cells,1)
        if ((cells(i,1)==3 && sum(sum(ismember(List_modif(:,3:4),cells(i,4:6)),2)==2)>0)...
           ||(cells(i,1)==4 && sum(sum(ismember(List_modif(:,3:4),cells(i,4:7)),2)==2)>0))
            if cells(i,1)==3
                nn=find(sum(ismember(List_modif(:,3:4),cells(i,4:6)),2)==2);
            else
                nn=find(sum(ismember(List_modif(:,3:4),cells(i,4:7)),2)==2);
            end
            for j=i+1:size(cells,1)
                if((cells(j,1)==3 && sum(ismember(List_modif(nn,3:4),cells(j,4:6)),2)==2)...
                  ||(cells(j,1)==4 && sum(ismember(List_modif(nn,3:4),cells(j,4:7)),2)==2))
                    cells(i,(find(cells(i,:)==-1,1)))=j-1;
                    cells(j,(find(cells(j,:)==-1,1)))=i-1;
                end
            end
        end
    end
else
    edges2=edges;
end


%check if there is still problems in edges (BC = 0 & -1 as neighbour)
erreur2b=[];
for i=1:size(edges2,1)
      if (edges2(i,3)==0 && edges2(i,5)==-1)
              erreur2b=[erreur2b,i ];
      end
end

%check if cells file is ok 
erreur3=[];
for i=1:size(cells,1)
    for j=i:size(cells,1)
        if i~=j
            erreur3=[erreur3,find(cells(i,2)==cells(j,2)&&cells(i,3)==cells(j,3)) ];
        end
    end
    if cells(i,1)==3
        if cells(i,4)==cells(i,5) || cells(i,4)==cells(i,6) || cells(i,6)==cells(i,5)
            erreur3=[erreur3,i];
    %    Comment to avoid corners od the grid with 2 '-1' Neighbours
    %    elseif cells(i,7)==cells(i,8) || cells(i,7)==cells(i,9) || cells(i,8)==cells(i,9)
            %erreur3=[erreur3,i];
        end
    else
        if cells(i,4)==cells(i,5) || cells(i,4)==cells(i,6) || cells(i,6)==cells(i,5)...
          || cells(i,4)==cells(i,7) || cells(i,5)==cells(i,7) || cells(i,6)==cells(i,7)
            erreur3=[erreur3,i];
        %Comment because not observed
        %elseif cells(i,8)==cells(i,9) || cells(i,8)==cells(i,10) || cells(i,9)==cells(i,10)...
        %  || cells(i,8)==cells(i,11) || cells(i,9)==cells(i,11) || cells(i,10)==cells(i,11)
        %    erreur3=[erreur3,i];
        end
    end
end

%correction of the cells
if ~isempty(erreur3)
    %Correction if edges of the internal boundary in double
    cells2=cells;
    Modif=0;
    I=1;
    List_modif=[];% edge deleted (location in edge file), edge conserved (location in edge file),
    % points associated to this edge (point1,point2))
    for ii=1:length(erreur3)
        i=erreur3(ii);
        if cells(i,1)==4
            cells2(i,1)=3;
            cells2(i,4:6)=unique(cells(i,4:7));
            II=find(cells(i,:)~=-1);
            cells2(i,7:9)=cells(i,II(end-2:end));
            cells2(i,10:11)=[NaN,NaN];
        end
    end
else
    cells2=cells;
end

% 
% %points2=points;edges2=edges;cells2=cells;
% %plot to verify
figure
color=['k','g','b','r'];

for ii=1:size(points2,1)
    hold on
  plot(points2(ii,1), points2(ii,2),'o')
end

for ii=1:size(edges2,1)
    hold on
  plot(points2([edges2(ii,1)+1,edges2(ii,2)+1],1),points2([edges2(ii,1)+1,edges2(ii,2)+1],2),color(edges2(ii,3)+1))
end
for ii=1:size(cells2,1)
    hold on 
    plot(cells2(ii,2), cells2(ii,3), '.r')
end


% %modification of files names:
% movefile('cells.dat', 'cells.dat_before');
% movefile('edges.dat', 'edges.dat_before');
% movefile('points.dat', 'points.dat_before');
%

%save to files
fid = fopen('edges.dat', 'w');
for i=1:size(edges2,1)
    for j=1:size(edges2,2)
        fprintf(fid,'%d ',edges2(i,j));
    end
    fprintf(fid,'\n');
end
fclose(fid)

fid = fopen('points.dat', 'w');
for i=1:size(points2,1)
    for j=1:size(points2,2)
        fprintf(fid,'%d ',points2(i,j));
    end
    fprintf(fid,'\n');
end
fclose(fid)

fid = fopen('cells.dat', 'w');
for i=1:size(cells2,1)
    for j=1:length(cells2(i,:))
        if ~isnan(cells2(i,j))
            fprintf(fid,'%d ',cells2(i,j));
        end
    end
    fprintf(fid,'\n');
end
fclose(fid)
















