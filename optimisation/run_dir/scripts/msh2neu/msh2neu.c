#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <limits.h>
#include <time.h>
#include "message.h"
#include "heap.h"
#include "macros.h"

typedef struct
{
	double x;
	double y;
	long int boundary_edge1; // Edge referennce of one boundary edge connected to this node (if any)
	long int boundary_edge2; // Edge referennce of the other boundary edge connected to this node (if any)
} Node;

long int num_nodes=0;
Node *nodes=NULL;

typedef struct
{
	int boundary;  // true if this edge is on a boundary, false otherwise
	long int node1;
	long int node2;
	long int element1;  // One element adjacent to this edge
	long int other;  // The other adjacent element, or a boundary reference
	long int group;  // The group reference, which only applies for an internal edge
} Edge;

long int num_edges=0;
Edge *edges=NULL;

typedef struct
{
	char *tag;
	long int num_nodes;
	long int *node_list;  // Note: the first node must be on one end of the boundary (if there is an end), but the remaining nodes may be in any order. Discontinuous boundaries are illegal.

	long int num_edges;
	long int *edge_list; // The list of edge references for this boundary, in any order
} Boundary;

long int num_boundaries=0;
Boundary *boundaries=NULL;

typedef struct
{
	int N;  // 3 for triangle or 4 for quadrilateral
	
	long int edges[4]; // Equal to -1 if unused

	long int nodes[4][2];

	long int node1;
	long int node2;
	long int node3;
	long int node4; // Equal to node1 for a triangle

	long int group;
} Element;

long int num_elements=0;
Element *elements=NULL;

typedef struct
{
	char *tag;
	long int num_elements;
	long int *element_list;
} Group;

long int num_groups=0;
Group *groups=NULL;


// Output a Gambit neutral file, based on the information in the global data structures.
void output_neutral_file(FILE *dest, char *source_name, char *dest_name)
{
	time_t tm;
	long int i, index;

	if (dest==NULL) woops(WOOPS_DATA, "Output file invalid");

	fprintf(dest, "        CONTROL INFO 2.2.30\n");
	fprintf(dest, "** GAMBIT NEUTRAL FILE\n");
	fprintf(dest, "%s (converted from %s using msh2neu)\n", dest_name, source_name);
	fprintf(dest, "PROGRAM:                Gambit     VERSION:  2.2.30\n");
	tm=time(NULL);
	fprintf(dest, "%s", asctime(localtime(&tm)));
	fprintf(dest, "     NUMNP     NELEM     NGRPS    NBSETS     NDFCD     NDFVL\n");
	fprintf(dest, "%10ld %9ld %9ld %9ld %9d %9d\n", num_nodes, num_elements, num_groups, num_boundaries, 2, 2);
	fprintf(dest, "ENDOFSECTION\n");

	fprintf(dest, "   NODAL COORDINATES 2.2.30\n");
	for (i=0; i<num_nodes; i++)
	{
		fprintf(dest, "%10ld %19.11le %19.11le\n", i+1, nodes[i].x, nodes[i].y);
	}
	fprintf(dest, "ENDOFSECTION\n");

	fprintf(dest, "      ELEMENTS/CELLS 2.2.30\n");
	for (i=0; i<num_elements; i++)
	{
		fprintf(dest, "%8ld %2d %2d %8ld %7ld %7ld %7ld\n", i+1, (elements[i].N==4 ? 2 : 3), elements[i].N, elements[i].node1+1, elements[i].node2+1, elements[i].node3+1, elements[i].node4+1);
	}
	fprintf(dest, "ENDOFSECTION\n");

	for (i=0; i<num_groups; i++)
	{
		fprintf(dest, "       ELEMENT GROUP 2.2.30\n");
		fprintf(dest, "GROUP: %10ld ELEMENTS:  %10ld MATERIAL:          2 NFLAGS:          1\n", i+1, groups[i].num_elements);
		fprintf(dest, "%32s\n", groups[i].tag);
		fprintf(dest, "       0\n");
		for (index=0; index<groups[i].num_elements; index++)
		{
			fprintf(dest, " %7ld", groups[i].element_list[index]+1);
			if ((index+1)%10==0 || index==groups[i].num_elements-1) fprintf(dest, "\n");
		}
		fprintf(dest, "ENDOFSECTION\n");
	}

	for (i=0; i<num_boundaries; i++)
	{
		fprintf(dest, " BOUNDARY CONDITIONS 2.2.30\n");
		fprintf(dest, "%32s %7d %7ld %7d %7d\n", boundaries[i].tag, 0, boundaries[i].num_nodes, 0, 24);
		for (index=0; index<boundaries[i].num_nodes; index++)
		{
			fprintf(dest, "%10ld\n", boundaries[i].node_list[index]+1);
		}
		fprintf(dest, "ENDOFSECTION\n");
	}

	return;
}

// Fill in the list of elements making up each group (order IS NOT important)
void fill_groups(void)
{
	long int el, gp;

	// Generate the counts of elements in each group
	for (el=0; el<num_elements; el++)
	{
		(groups[elements[el].group].num_elements)++;
		groups[elements[el].group].element_list=(long int *)srealloc((void *)groups[elements[el].group].element_list, sizeof(long int)*groups[elements[el].group].num_elements, HEAP_DATA);

		groups[elements[el].group].element_list[groups[elements[el].group].num_elements-1]=el;
	}

	for (gp=0; gp<num_groups; gp++)
	{
		message("Group %ld \"%s\": elements ", gp, groups[gp].tag);
		for (el=0; el<groups[gp].num_elements; el++)
		{
			message("%ld ", groups[gp].element_list[el]);
		}
		message("\n");
	}

	return;
}

// Fill in the list of nodes making up each boundary (order IS important)
void fill_boundaries(void)
{
	long int ed, nod, bound, index, count, first_node, end_node;

	// First, traverse the edge list and fill in the boundary edge connections for the node list
	for (ed=0; ed<num_edges; ed++)
	{
		if (edges[ed].boundary==true)
		{
			if (edges[ed].node1<0 || edges[ed].node1>=num_nodes)  woops(WOOPS_DATA, "Invalid node reference 1 (node %ld of %ld)", edges[ed].node1, num_nodes);
			if (edges[ed].node2<0 || edges[ed].node2>=num_nodes) woops(WOOPS_DATA, "Invalid node reference 2");
			if (nodes[edges[ed].node1].boundary_edge1<0) nodes[edges[ed].node1].boundary_edge1=ed;
			else if (nodes[edges[ed].node1].boundary_edge2<0) nodes[edges[ed].node1].boundary_edge2=ed;
			else woops(WOOPS_DATA, "More than 2 boundary edges meeting at a node");
			if (nodes[edges[ed].node2].boundary_edge1<0) nodes[edges[ed].node2].boundary_edge1=ed;
			else if (nodes[edges[ed].node2].boundary_edge2<0) nodes[edges[ed].node2].boundary_edge2=ed;
			else woops(WOOPS_DATA, "More than 2 boundary edges meeting at a node");
		}
	}

	for (nod=0; nod<num_nodes; nod++)
	{
		if (nodes[nod].boundary_edge1>=0 && nodes[nod].boundary_edge1<num_edges && nodes[nod].boundary_edge2>=0 && nodes[nod].boundary_edge2<num_edges)
		{
			message("Node %ld connects to boundary edges %ld and %ld\n", nod, nodes[nod].boundary_edge1, nodes[nod].boundary_edge2);
		}
		else message("Node %ld is internal\n", nod);
	}

	// Traverse the edge list to fill in the edge tables for the boundaries
	for (ed=0; ed<num_edges; ed++)
	{
		if (edges[ed].boundary==true)
		{
			boundaries[edges[ed].other].num_edges++;
			boundaries[edges[ed].other].edge_list=(long int *)srealloc((void *)boundaries[edges[ed].other].edge_list, sizeof(long int)*boundaries[edges[ed].other].num_edges, HEAP_DATA);
			boundaries[edges[ed].other].edge_list[boundaries[edges[ed].other].num_edges-1]=ed;
		}
	}
	for (bound=0; bound<num_boundaries; bound++)
	{
		message("Boundary %ld \"%s\" consists of edges ", bound, boundaries[bound].tag);
		for (ed=0; ed<boundaries[bound].num_edges; ed++)
		{
			message("%ld ", boundaries[bound].edge_list[ed]);
		}
		message("\n");
	}

	// Convert the edge list into a node list
	for (bound=0; bound<num_boundaries; bound++)
	{
		for (index=0; index<boundaries[bound].num_edges; index++)
		{
			ed=boundaries[bound].edge_list[index];

			nod=edges[ed].node1;
			// Insert it in the list
			for (count=0; count<boundaries[bound].num_nodes; count++)
			{
				if (boundaries[bound].node_list[count]==nod) goto jump1;
			}
			boundaries[bound].num_nodes++;
			boundaries[bound].node_list=(long int *)srealloc((void *)boundaries[bound].node_list, boundaries[bound].num_nodes*sizeof(long int), HEAP_DATA);
			boundaries[bound].node_list[boundaries[bound].num_nodes-1]=nod;

			jump1:
			nod=edges[ed].node2;
			// Insert it in the list
			for (count=0; count<boundaries[bound].num_nodes; count++)
			{
				if (boundaries[bound].node_list[count]==nod) goto jump2;
			}
			boundaries[bound].num_nodes++;
			boundaries[bound].node_list=(long int *)srealloc((void *)boundaries[bound].node_list, boundaries[bound].num_nodes*sizeof(long int), HEAP_DATA);
			boundaries[bound].node_list[boundaries[bound].num_nodes-1]=nod;

			jump2:
			nod=nod; // Keep the compiler happy
		}
	}

	// For each boundary, locate one end node (if there is one) and swap it to the start of the list
	for (bound=0; bound<num_boundaries; bound++)
	{
		// Starting from any node, walk around the boundary chain from node to edge to node, until either the first node is reached (proving the
		// boundary forms a closed loop) or else we cannot proceed further without leaving the bounadry.
		first_node=boundaries[bound].node_list[0];
		ed=-1;
		nod=first_node;
		do
		{
			// Find the next connecting edge
			if (nodes[nod].boundary_edge1!=ed) ed=nodes[nod].boundary_edge1;
			else if (nodes[nod].boundary_edge2!=ed) ed=nodes[nod].boundary_edge2;
			else woops(WOOPS_DATA, "Edge connection duplicated");

			// Check the edge is on a boundary
			if (edges[ed].boundary!=true) woops(WOOPS_DATA, "Mesh data is corrupt");

			// If this edge is part of a different boundary, then nod must be an end node.
			if (edges[ed].other!=bound)
			{
				break;
			}

			// Find the next connecting node
			if (edges[ed].node1!=nod) nod=edges[ed].node1;
			else if (edges[ed].node2!=nod) nod=edges[ed].node2;
			else woops(WOOPS_DATA, "Node connection duplicated");
		} while (nod!=first_node);
		end_node=nod;

		// Locate end_node in the node list for the boundary, and swap it with the first node in the list
		for (index=0; index<boundaries[bound].num_nodes; index++)
		{
			if (boundaries[bound].node_list[index]==end_node)
			{
				boundaries[bound].node_list[index]=boundaries[bound].node_list[0];
				boundaries[bound].node_list[0]=end_node;
				goto found_it;
			}
		}
		woops(WOOPS_DATA, "Node not found!");

		found_it:
		end_node=end_node; // Keep the compiler happy!
	}

	for (bound=0; bound<num_boundaries; bound++)
	{
		message("Boundary %ld \"%s\" consists of nodes ", bound, boundaries[bound].tag);
		for (ed=0; ed<boundaries[bound].num_nodes; ed++)
		{
			message("%ld ", boundaries[bound].node_list[ed]);
		}
		message("\n");
	}


	return;
}

// Build the list of elements using the data loaded from the msh file
void build_elements(void)
{
	long int ed, el, i;

	// Allocate and wipe the data structure
	elements=(Element *)smalloc(sizeof(Element)*num_elements, HEAP_DATA);

	for (el=0; el<num_elements; el++)
	{
		elements[el].edges[0]=-1;
		elements[el].edges[1]=-1;
		elements[el].edges[2]=-1;
		elements[el].edges[3]=-1;
		elements[el].N=0;
	}

	// Put the edge references into the element list
	for (ed=0; ed<num_edges; ed++)
	{
		if (edges[ed].boundary==true)
		{
			el=edges[ed].element1;
			for (i=0; i<=3; i++)
			{
				if (elements[el].edges[i]<0)
				{
					elements[el].edges[i]=ed;
					break;
				}
			}
		}
		else
		{
			el=edges[ed].element1;
			for (i=0; i<=3; i++)
			{
				if (elements[el].edges[i]<0)
				{
					elements[el].edges[i]=ed;
					break;
				}
			}

			el=edges[ed].other;
			for (i=0; i<=3; i++)
			{
				if (elements[el].edges[i]<0)
				{
					elements[el].edges[i]=ed;
					break;
				}
			}
		}
	}

	// Compute the (ordered) node list for each element
	for (el=0; el<num_elements; el++)
	{
		// Compute triangle / quad
		for (i=0; i<=3; i++)
		{
			ed=elements[el].edges[i];
			if (ed>=0 && ed<num_edges)
			{
				elements[el].N++;
				elements[el].nodes[i][0]=edges[ed].node1;
				elements[el].nodes[i][1]=edges[ed].node2;
			}
		}

		if (elements[el].N==3)
		{
			elements[el].node1=elements[el].nodes[0][0];
			elements[el].node2=elements[el].nodes[0][1];
			// Need to find the third node, which is different to the first two
			if (elements[el].nodes[1][0]!=elements[el].nodes[0][0] && elements[el].nodes[1][0]!=elements[el].nodes[0][1]) elements[el].node3=elements[el].nodes[1][0];
			else if (elements[el].nodes[1][1]!=elements[el].nodes[0][0] && elements[el].nodes[1][1]!=elements[el].nodes[0][1]) elements[el].node3=elements[el].nodes[1][1];
			else woops(WOOPS_DATA, "Something is screwed!");

				// Fourth node is the first one, repeated
			elements[el].node4=elements[el].nodes[0][0];

			message("Element %ld (tri): nodes %ld %ld %ld (%ld)\n", el, elements[el].node1, elements[el].node2, elements[el].node3, elements[el].node4);
		}
		else if (elements[el].N==4)
		{
			elements[el].node1=elements[el].nodes[0][0];
			elements[el].node2=elements[el].nodes[0][1];
			// Need to find the edge which shares node2 (but not node1)
			for (i=0; i<=3; i++)
			{
				ed=elements[el].edges[i];
				if (edges[ed].node1==elements[el].node2 && edges[ed].node2!=elements[el].node1) elements[el].node3=edges[ed].node2;
				else if (edges[ed].node2==elements[el].node2 && edges[ed].node1!=elements[el].node1) elements[el].node3=edges[ed].node1;
			}
			// Need to find the edge which shares node3 (but not node2)
			for (i=0; i<=3; i++)
			{
				ed=elements[el].edges[i];
				if (edges[ed].node1==elements[el].node3 && edges[ed].node2!=elements[el].node2) elements[el].node4=edges[ed].node2;
				else if (edges[ed].node2==elements[el].node3 && edges[ed].node1!=elements[el].node2) elements[el].node4=edges[ed].node1;

			}
			message("Element %ld (quad): nodes %ld %ld %ld %ld\n", el, elements[el].node1, elements[el].node2, elements[el].node3, elements[el].node4);
		}
		else woops(WOOPS_DATA, "Element %ld is an illegal polygon (N=%d)", el, elements[el].N);
	}

	// Compute the group membership of each element
	for (el=0; el<num_elements; el++)
	{
		elements[el].group=-1;
		for (i=0; i<=3; i++)
		{
			ed=elements[el].edges[i];
			if (ed>=0 && ed<num_edges)
			{
				if (edges[ed].group>=0 && edges[ed].group<num_groups)
				{
					elements[el].group=edges[ed].group;
				}
			}
		}
		message("Element %ld is in group %ld (\"%s\")\n", el, elements[el].group, groups[elements[el].group].tag);
	}

	return;
}

// Checks to see if there is already a boundary with the nominated tag, adding a new boundary if required. The boundary reference is returned in both cases.
long int add_boundary(char *tag)
{
	long int i;

	for (i=0; i<num_boundaries; i++)
	{
		if (strcmp(tag, boundaries[i].tag)==0)
		{
			return i;
		}
	}

	num_boundaries++;
	boundaries=(Boundary*)srealloc((void *)boundaries, num_boundaries*sizeof(Boundary), HEAP_DATA);

	boundaries[num_boundaries-1].num_nodes=0;
	boundaries[num_boundaries-1].node_list=NULL;
	boundaries[num_boundaries-1].num_edges=0;
	boundaries[num_boundaries-1].edge_list=NULL;
	boundaries[num_boundaries-1].tag=(char *)smalloc(sizeof(char)*(strlen(tag)+1), HEAP_DATA);
	strcpy(boundaries[num_boundaries-1].tag, tag);

	message("Boundary %ld \"%s\":\n", num_boundaries-1, boundaries[num_boundaries-1].tag);

	return num_boundaries-1;
}

// Checks to see if there is already a group with the nominated tag, adding a new group if required. The group reference is returned in both cases.
long int add_group(char *tag)
{
	long int i;

	for (i=0; i<num_groups; i++)
	{
		if (strcmp(tag, groups[i].tag)==0)
		{
			return i;
		}
	}

	num_groups++;
	groups=(Group*)srealloc((void *)groups, num_groups*sizeof(Group), HEAP_DATA);

	groups[num_groups-1].num_elements=0;
	groups[num_groups-1].element_list=NULL;
	groups[num_groups-1].tag=(char *)smalloc(sizeof(char)*(strlen(tag)+1), HEAP_DATA);
	strcpy(groups[num_groups-1].tag, tag);

	message("Group %ld \"%s\":\n", num_groups-1, groups[num_groups-1].tag);

	return num_groups-1;
}

// Scans the source file into the global data structures, without any further processing.
void scan(FILE *source)
{
	long int temp;
	char line[MAX_LINE_LENGTH+1];
	char current_tag[MAX_LINE_LENGTH+1];
	int in_named_zone=false;

	if (source==NULL) woops(WOOPS_DATA, "Input file invalid");

	do
	{
		re_read:
		fgets(line, MAX_LINE_LENGTH, source);
		if (strncmp(line, "(0 ", 3)==0) // Comment
		{
			long int i, ii;
			for (i=0; i<strlen(line); i++)
			{
				if (strncmp(line+i, "zone ", 5)==0)  // Zone labelling comment
				{
					ii=0;
					while (line[i+5+ii]!='\"') ii++;
					strncpy(current_tag, line+5+i, ii);
					current_tag[ii]='\0';

					// Find out if this is a named zone, or a default classification

					if (strncmp(line+4, "Interior", 8)==0) in_named_zone=true;
					else in_named_zone=false;

					break;
				}
			}
			goto re_read;  // Comment
		}
		if (strncmp(line, "(1 ", 3)==0) goto re_read;  // Header

		if (strncmp(line, "(2 ", 3)==0)  // Dimensions
		{
			if (strncmp(line, "(2 2)", 5)==0)
			{
				message("Mesh is two dimensional, as required.\n");
			}
			else if (strncmp(line, "(2 3)", 5)==0)
			{
				woops(WOOPS_DATA, "Mesh is three dimensional, which is not supported!\n");
			}
			else
			{
				woops(WOOPS_DATA, "Invalid grid section!\n");
			}
		}
		
		if (strncmp(line, "(10 ", 4)==0)  // Node section
		{
			sscanf(line+5, "%lx", &temp);
			if (temp==0) goto re_read;  // Total number of nodes information
			else if (temp>0)  // Node zone
			{
				fgets(line, MAX_LINE_LENGTH, source);
				if (line[0]=='(') fgets(line, MAX_LINE_LENGTH, source);  // Drop the line starting with a bracket
				
				while (strncmp(line, "))", 2)!=0)
				{
					num_nodes++;
					nodes=(Node *)srealloc((void *)nodes, num_nodes*sizeof(Node), HEAP_DATA);

					nodes[num_nodes-1].boundary_edge1=-1;
					nodes[num_nodes-1].boundary_edge2=-1;

					sscanf(line, "%lf %lf", &(nodes[num_nodes-1].x), &(nodes[num_nodes-1].y));

					message("Node %ld: (%lf, %lf)\n", num_nodes-1, nodes[num_nodes-1].x, nodes[num_nodes-1].y);

					fgets(line, MAX_LINE_LENGTH, source);
				} 
			}
			else woops(WOOPS_DATA, "Illegal node zone");
		}

		if (strncmp(line, "(13 ", 4)==0)  // "Face" section (actually concerning edges)
		{
			sscanf(line+5, "%lx", &temp);
			if (temp==0) goto re_read;  // Total number of faces information
			else if (temp>0)  // Face zone
			{
				fgets(line, MAX_LINE_LENGTH, source);
				if (line[0]=='(') fgets(line, MAX_LINE_LENGTH, source);  // Drop the line starting with a bracket
				
				while (strncmp(line, ")", 1)!=0)
				{
					num_edges++;
					edges=(Edge *)srealloc((void *)edges, num_edges*sizeof(Edge), HEAP_DATA);
					
					sscanf(line, "%lx %lx %lx %lx", &(edges[num_edges-1].node1), &(edges[num_edges-1].node2), &(edges[num_edges-1].element1), &(edges[num_edges-1].other));

					num_elements=MAX(num_elements, edges[num_edges-1].element1);
					num_elements=MAX(num_elements, edges[num_edges-1].other);

					edges[num_edges-1].node1--;
					edges[num_edges-1].node2--;
					edges[num_edges-1].element1--;
					edges[num_edges-1].other--;

					if (edges[num_edges-1].other<0) 
					{
						edges[num_edges-1].boundary=true;
						
						edges[num_edges-1].other=add_boundary(current_tag);

						edges[num_edges-1].group=-1;

						message("Edge %ld: (node %ld - node %ld), adjacent to element %ld and on \"%s\"\n", num_edges-1, edges[num_edges-1].node1, edges[num_edges-1].node2, edges[num_edges-1].element1, current_tag);

					}
					else 
					{
						edges[num_edges-1].boundary=false;

						if (in_named_zone==true)
						{
							edges[num_edges-1].group=add_group(current_tag);
							message("Edge %ld: (node %ld - node %ld), adjacent to elements %ld and %ld (in group \"%s\")\n", num_edges-1, edges[num_edges-1].node1, edges[num_edges-1].node2, edges[num_edges-1].element1, edges[num_edges-1].other, current_tag);
						}
						else
						{
							edges[num_edges-1].group=-1; // Signal that this edge is on a classification boundary (internal edge, on the interface between two named zones)
							message("Edge %ld: (node %ld - node %ld), adjacent to elements %ld and %ld (on an interface between groups)\n", num_edges-1, edges[num_edges-1].node1, edges[num_edges-1].node2, edges[num_edges-1].element1, edges[num_edges-1].other);
						}

					}

					fgets(line, MAX_LINE_LENGTH, source);
				} 
				if (strncmp(line, "))", 2)!=0) fgets(line, MAX_LINE_LENGTH, source);  // Eat another line if only one closing bracket found
			}
			else woops(WOOPS_DATA, "Illegal node zone");
		}
	} while (!feof(source));

	if (num_nodes<1) woops(WOOPS_DATA, "Nodes not read succesfully");

	return;
}

void process(FILE *source, FILE *dest, char *source_name, char *dest_name)
{
	// Read the source file, filling in all available information in the global data structures
	scan(source);

	// Process the data in the data structures, in order to complete them
	build_elements();
	fill_groups();
	fill_boundaries();

	// Output the neutral file
	output_neutral_file(dest, source_name, dest_name);

	return;
}

// Free all the memory in the data structures
void free_memory(void)
{
	long int i;

	for (i=0; i<num_boundaries; i++)
	{
		sfree(boundaries[i].tag, HEAP_DATA);
		sfree(boundaries[i].node_list, HEAP_DATA);
		sfree(boundaries[i].edge_list, HEAP_DATA);
	}

	for (i=0; i<num_groups; i++)
	{
		sfree(groups[i].tag, HEAP_DATA);
		sfree(groups[i].element_list, HEAP_DATA);
	}

	sfree(boundaries, HEAP_DATA);
	sfree(edges, HEAP_DATA);
	sfree(elements, HEAP_DATA);
	sfree(nodes, HEAP_DATA);
	sfree(groups, HEAP_DATA);

	return;
}

int main(int argc, char *argv[])
{
	FILE *source, *dest;

	// Check the argument validity

	if (argc!=3)
	{
		fprintf(stderr, "Usage: msh2neu source.msh output.neu\n");
		fprintf(stderr, "Converts the file source.msh (from IcemCFD \"Output to Fluent V6\") to output.neu (Gambit neutral file)\n");
		exit(-1);
	}

	initialise_messages();

	// Open the files

	source=fopen(argv[1], "r");
	if (source==NULL) woops(WOOPS_DATA, "Could not read file \"%s\"\n", argv[1]);

	dest=fopen(argv[2], "w");
	if (dest==NULL) woops(WOOPS_DATA, "Could not write to file \"%s\"\n", argv[2]);

	// Process the files
	
	process(source, dest, argv[1], argv[2]);

	// Close the files

	fclose(source);
	fclose(dest);

	free_memory();

	#ifdef HEAP_DEBUG_MODE_ACTIVE
	// Check for memory leaks
	close_heap();
	#endif

	close_messages();

	return 0;
}

