#!/bin/bash

# This launches all the array runs

#SBATCH -J TestSUNTANS
#SBATCH -A uoo00038
#SBATCH --time=00:40:00
#SBATCH --mem-per-cpu=512
#SBATCH --ntasks 16
#SBATCH -C sb
#SBATCH -o testing_%j.out
#SBATCH -e testing_%j.err
#SBATCH --array=0

module load Python/3.4.1-goolf-1.5.14
module load netCDF/4.2-goolf-1.5.14
module load MATLAB

cd /projects/uoo00038/run_dir/
cp -r test-constrained $SCRATCH_DIR
python /projects/uoo00038/run_dir/Parameters_and_Launch.py $SLURM_ARRAY_TASK_ID
