#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include "message.h"

#define ERROR_LOG_FILE_NAME "errors"
#define MESSAGE_LOG_FILE_NAME "messages"
#define STATUS_LOG_FILE_NAME "status"

FILE *error_log_file=NULL;
FILE *message_log_file=NULL;
FILE *status_log_file=NULL;

/* 
Generic error function:
 
Call in this manner: 
woops(WOOPS_DATA, "Out of memory");
  or
woops(WOOPS_DATA, "Ran out of memory when attempting to allocate block of size %ld", block_size);
  or
woops(WOOPS_DATA, "Unknown item (%d, %s)", item_number, item_name);
  etc
 
  The error message goes to the error log file, NOT the terminal.

*/
void woops(char file[], long int line, char date[], char time[], char formatstring[], ...)
{
	va_list list;

//	status("Terminated (error)");
	message("An error occurred!");

//	if (error_log_file==NULL) visible_woops(file, line, date, time, "\nError log file \"%s\" not open\n(Source File: %s, Line Number: %ld, Build Date: %s, Build Time: %s)\n", ERROR_LOG_FILE_NAME); 
	
	va_start(list, formatstring);

	fprintf(stderr, "\n");
	vfprintf(stderr, formatstring, list);
	fprintf(stderr, "\n(Source File: %s, Line Number: %ld, Build Date: %s, Build Time: %s)\n", file, line, date, time);

	va_end(list);
	
	//close_keyboard();  // Resets the keyboard to normal mode, if necessary

	fflush(stderr);

	exit(-1);
}

// Like woops, but output goes to the terminal (if any) instead
void visible_woops(char file[], long int line, char date[], char time[], char formatstring[], ...)
{
	va_list list;

	va_start(list, formatstring);

	fprintf(stderr, "\n");
	vfprintf(stderr, formatstring, list);
	fprintf(stderr, "\n(Source File: %s, Line Number: %ld, Build Date: %s, Build Time: %s)\n", file, line, date, time);

	va_end(list);
	
	//close_keyboard();  // Resets the keyboard to normal mode, if necessary

	fflush(stderr);

	exit(-1);
}

// 
// Prints an arbitrary message
// 
// These messages go to the message log file, NOT the terminal.
//
void message(char formatstring[], ...)
{
	va_list list;

	if (message_log_file==NULL)
	{
		fprintf(stderr, "Message log file \"%s\" not open\n(Source File: %s, Line Number: %ld, Build Date: %s, Build Time: %s)\n", MESSAGE_LOG_FILE_NAME,  __FILE__, __LINE__, __DATE__, __TIME__);
		exit(-1);
	}
	
	va_start(list, formatstring);
	
	vfprintf(message_log_file, formatstring, list);

	va_end(list);

	fflush(message_log_file);

	return;
}

// 
// Prints an arbitrary message
// 
// These messages go to the terminal, if there still is one.
//
void visible_message(char formatstring[], ...)
{
	va_list list;

	va_start(list, formatstring);
	
	vfprintf(stderr, formatstring, list);

	va_end(list);

	fflush(stderr);

	return;
}

// Output the status message to the status file
// (Contents of the file are overwritten by subsequent calls)
void status(char formatstring[], ...)
{
	va_list list;

	status_log_file=fopen(STATUS_LOG_FILE_NAME, "w");
	if (status_log_file==NULL) 
	{
		fprintf(stderr,  "Could not open status log file  \"%s\"\n(Source File: %s, Line Number: %ld, Build Date: %s, Build Time: %s)\n", STATUS_LOG_FILE_NAME,  __FILE__, __LINE__, __DATE__, __TIME__);
		exit(-1);
	}
	
	va_start(list, formatstring);
	
	vfprintf(status_log_file, formatstring, list);

	va_end(list);

	fclose(status_log_file);

	return;
}

// Open all the message streams
void initialise_messages(void)
{
	/*
	error_log_file=fopen(ERROR_LOG_FILE_NAME, "w");
	if (error_log_file==NULL) 
	{
		fprintf(stderr, "\nCould not open error log file  \"%s\"\n(Source File: %s, Line Number: %ld, Build Date: %s, Build Time: %s)\n", ERROR_LOG_FILE_NAME, __FILE__, __LINE__, __DATE__, __TIME__);
		exit(-1);
	}
	*/

	message_log_file=fopen(MESSAGE_LOG_FILE_NAME, "w");
	if (message_log_file==NULL)
	{
		fprintf(stderr,  "Could not open message log file  \"%s\"\n(Source File: %s, Line Number: %ld, Build Date: %s, Build Time: %s)\n", MESSAGE_LOG_FILE_NAME,  __FILE__, __LINE__, __DATE__, __TIME__);
		exit(-1);
	}

	return;
}

// Close all the message streams
void close_messages(void)
{
	fclose(message_log_file);
//	fclose(error_log_file);

	return;
}



