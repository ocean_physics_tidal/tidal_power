#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <float.h>
#include <complex.h>
#include <sys/types.h>

#include "neu2msh.h"
#include "macros.h"
#include "message.h"
#include "heap.h"
#include "mesh.h"

void output_msh(FILE *dest, char *source_name, char *dest_name)
{
	long int i, ii, ed, zone_counter, group_edges, counted_edges, counted_elements;

	// Header stuff

	fprintf(dest, "(0 \"%s - converted from Gambit neutral file %s using neu2msh\")\n", dest_name, source_name);

	fprintf(dest, "(2 2)\n");

	// Node section

	fprintf(dest, "(10 (0 1 %lx 1 2))\n", num_nodes);

	fprintf(dest, "(10 (6 1 %lx 1 2)(\n", num_nodes);
	for (i=0; i<num_nodes; i++)
	{
		fprintf(dest, "%.9le %.9le\n", nodes[i].x, nodes[i].y);
	}
	fprintf(dest, "))\n");

	// Element section

	fprintf(dest, "(12 (0 1 %lx 0 0))\n", num_elements);

	zone_counter=7;
	counted_elements=0;
	for (i=0; i<num_groups; i++)
	{
		fprintf(dest, "(12 (%lx %lx %lx 1 0)\n(", zone_counter, counted_elements+1, counted_elements+groups[i].num_elements);
		for (ii=0; ii<groups[i].num_elements; ii++)
		{
			fprintf(dest, " %s", (elements[groups[i].elements[ii]].N==3 ? "1" : "3"));
			if ((ii+1)%20==0) fprintf(dest, "\n");
		}
		if ((ii)%20!=0) fprintf(dest, "\n");
		fprintf(dest, "))\n");
		zone_counter++;
		counted_elements+=groups[i].num_elements;
	}

	// Face section

	fprintf(dest, "(13 (0 1 %lx 0 0))\n", num_edges);

	// Internal edges in one group only

	counted_edges=0;
	for (i=0; i<num_groups; i++)
	{
		// First, count the number of edges belonging (exclusively) to this group
		group_edges=0;
		for (ii=0; ii<num_edges; ii++)
		{
			if (edges[ii].boundary==false)
			{
				if (elements[edges[ii].element1].group==i && elements[edges[ii].other].group==i)
				{
					group_edges++;
				}
			}
		}
		if (group_edges>=1) 
		{
			fprintf(dest, "(0 \"Interior faces of zone %s\")\n", groups[i].tag);
			fprintf(dest, "(13 (%lx %lx %lx 2 2)\n(\n", zone_counter, counted_edges+1, counted_edges+group_edges);
			for (ii=0; ii<num_edges; ii++)
			{
				if (edges[ii].boundary==false)
				{
					if (elements[edges[ii].element1].group==i && elements[edges[ii].other].group==i)
					{
						fprintf(dest, "%lx %lx %lx %lx\n", edges[ii].nodes[0]+1, edges[ii].nodes[1]+1, edges[ii].element1+1, edges[ii].other+1);
					}
				}
			}
			fprintf(dest, ")\n)\n");
			zone_counter++;
			counted_edges+=group_edges;
		}
	}

	// Internal edges on the interface between two groups

	group_edges=0;
	for (ii=0; ii<num_edges; ii++)
	{
		if (edges[ii].boundary==false)
		{
			if (elements[edges[ii].element1].group!=elements[edges[ii].other].group)
			{
				group_edges++;
			}
		}
	}
	if (group_edges>=1)
	{
		fprintf(dest, "(0 \"Faces of zone GEOM\")\n");
		fprintf(dest, "(13 (%lx %lx %lx 3 2)\n(\n", zone_counter, counted_edges+1, counted_edges+group_edges);
		for (ii=0; ii<num_edges; ii++)
		{
			if (edges[ii].boundary==false)
			{
				if (elements[edges[ii].element1].group!=elements[edges[ii].other].group)
				{
					fprintf(dest, "%lx %lx %lx %lx\n", edges[ii].nodes[0]+1, edges[ii].nodes[1]+1, edges[ii].element1+1, edges[ii].other+1);
				}
			}
		}
		fprintf(dest, ")\n)\n");
		zone_counter++;
		counted_edges+=group_edges;
	}

	// Edges on each boundary

	for (i=0; i<num_boundaries; i++)
	{
		group_edges=boundaries[i].num_edges;
		if (group_edges>=1)
		{
			fprintf(dest, "(0 \"Faces of zone %s\")\n", boundaries[i].tag);
			fprintf(dest, "(13 (%lx %lx %lx 3 2)\n(\n", zone_counter, counted_edges+1, counted_edges+group_edges);
			for (ii=0; ii<boundaries[i].num_edges; ii++)
			{
				ed=boundaries[i].edges[ii];
				fprintf(dest, "%lx %lx %lx %lx\n", edges[ed].nodes[0]+1, edges[ed].nodes[1]+1, edges[ed].element1+1, 0);
			}
			fprintf(dest, ")\n)\n");
			zone_counter++;
			counted_edges+=group_edges;
		}
	}
 
	return;
}


int main(int argc, char *argv[])
{
	FILE *dest;

	initialise_messages();

	// Check the argument validity

	if (argc!=3)
	{
		fprintf(stderr, "Usage: neu2msh source.neu output.msh\n");
		fprintf(stderr, "Converts the file source.neu (Gambit neutral file) to output.msh (for IcemCFD \"Import Mesh from Fluent\")\n");
		exit(-1);
	}

	// Open the files

	load_mesh(argv[1]);

	dest=fopen(argv[2], "w");
	if (dest==NULL)
	{
		fprintf(stderr, "Could not write to file \"%s\"\n", argv[2]);
		exit(-1);
	}

	// Process the files
	
	output_msh(dest, argv[1], argv[2]);

	// Close the files

	destroy_mesh();
	fclose(dest);

	#ifdef HEAP_DEBUG_MODE_ACTIVE
	// Check for memory leaks
	close_heap();
	#endif

	close_messages();

	return 0;
}

