#!/bin/sh
########################################################################
#
# Shell script to run a suntans case.
# Modification of boundaries.sh by Alice Harang, 2/06/15
#
########################################################################

SUNTANSHOME=$1
SUN=$SUNTANSHOME/sun
#SUNPLOT=$SUNTANSHOME/sunplot

. $SUNTANSHOME/Makefile.in

maindatadir=rundata
data_dir=$2

NUMPROCS=$3

RUNWITH=$4

OPTION=$5
OPTION="${OPTION%\"}"
OPTION="${OPTION#\"}"

if [ -z "$MPIHOME" ] ; then
    EXEC=$SUN
else
  if [ $RUNWITH = "mpirun" ] ; then
    EXEC="$MPIHOME/bin/mpirun -np $NUMPROCS $SUN"
  else
    EXEC="srun $SUN"
  fi
fi

#if [ -z "$TRIANGLEHOME" ] ; then
#    echo Error: This example will not run without the triangle libraries.
#    echo Make sure TRIANGLEHOME is set in $SUNTANSHOME/Makefile.in
#    exit 1
#fi

if [ ! -d $data_dir ] ; then
    cp -r $maindatadir $data_dir
    echo Creating grid...
    $EXEC -g --datadir=$data_dir
#else
#    cp $maindatadir/suntans.dat $data_dir/.
fi

echo Running suntans...
echo $EXEC $OPTION --datadir=$data_dir
$EXEC $OPTION --datadir=$data_dir
