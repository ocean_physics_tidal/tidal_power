#! /usr/bin/env python3.4
##########################################################################
##Compute and save the simulations parameters        ##
##Launch the set of simulation associated to the parameters.      ##
##Parameters to explore:          ##
##  Parameters of the farm:          ##
##  - r3' : tunning parameter        ##
##  - epsilon' : density of turbines on a line      ##
##  - Number of rows and position of the turbines rows      ##
##  Other parameters:          ##
##  - number of processors          ##
##  - forcing phase difference                          ##
##  - forcing amplitude          ##
##IE: The exploration of the farm parameter are realised by restarting of a 'without    ##
##  turbines'configuration. For the other cases, a new initialisation of the computation  ##
##  is neededfor each parameter.                              ##
##########################################################################

import os
import math
import initial

option = {
'num_procs'				: [28],
'mesh_used'				: ['fluent-ttf-constr-Gauss'],
'Suntans_info'		: 0,  # 1 => option -vv, versatil / 0=> no info
'slurmnode'				: 0,	# if running with a SLURM script
'optimise'				: 1,
'regrid'					: 0,

#############Directories#######################
'dir_run'					: os.getcwd(),
'dir_sources_ext' : '/home/vaughanw/optimisation/suntans/',
'dir_init'				: 'init',							# classical directory for simulations
'dir_simu'				: 'test-constrained',	# base name for simu folder
'dir_sources'			: os.getcwd() + '/sources',

##############Forcing parameters##############
# Tidal signal:
'omega'						: 2 * math.pi / (12 * 3600),
'amplitude'				: [1.0],   # m
'phi'							: [10.0],  # deg

######Restart and convergence parameters#######
# run without turbines for initialisation:
'pdt_init'				: 3,
'n_conserve'			: 50,  # output of P

# runs with turbines:
'pdt_turb'				: 3,

# restart runs for convergence:
'dt_meanP'				: 3600 * 6,   # for test of convergence
'marge'						: 0.02,				# for test of convergence
'pdt_restart'			:  3,					# 3 for thin

####minimisation options####
'abstolerance'		:  0.01,  # absolute error tolerance
'minvalue'				:  0.3,   # minimum variable can be
'maxvalue'				:  0.98,  # maximum
'DX_f'						:  200.0  # wake length (fixed for all rows of a configuration)
} 

option['nsteps_init'] = 3600 * 48 / option['pdt_init']
option['nsteps_turb'] = 3600 * 48 / option['pdt_turb']
option['nsteps_restart'] = option['dt_meanP'] / option['pdt_restart']

initial.setup(option)
initial.launch_all(option)
