#! /usr/bin/env python3.4
# Launch different simulations to go through internal parameters:
# Organisation of the rows, properties of the turbines

import shutil
import os
from auto_restart import auto_restart
import fileops

def launch_power_curve_v2(folder, num_procs, option):

  dir_all = option['dir_scratch'] + '/' + option['dir_simu'] + '/' + folder

  r3 = option['r3']
  eps = option['eps']
  n_rows = option['n_rows']
  P = 0   # Power vector

  ############ For all simulations ##############
  new_dir = dir_all + '/sim_' + str(option['run_number']) + '/iter_' + str(option['iterno'])

  # creation of the new directory and copy of files in it
  os.makedirs(new_dir)
  os.chdir(new_dir)

  ffp = ['celldata.dat', 'edgedata.dat', 'topology.dat', 'edges.dat', 'cells.dat', 'nodes.dat']
  ff = ['vertspace.dat', 'points.dat', 'edges.dat', 'cells.dat', 'depth.dat-voro']

  for npr in range(num_procs):
    shutil.copy(dir_all + '/' + option['dir_init'] + '/store.dat.' + str(npr), 'start.dat.' + str(npr))
    for filename in ffp:
      os.symlink(dir_all + '/' + option['dir_init'] + '/' + filename + '.' + str(npr), filename + '.' + str(npr))
  for filename in ff:
    os.symlink(dir_all + '/' + option['dir_init'] + '/' + filename, filename)
  shutil.copy(dir_all + '/' + option['dir_init'] + '/suntans.dat', 'suntans.dat')

  ################################################
  # Modification of the tidal components and farm of
  # turbines properties in Suntans.dat
  turbinestring = 'n_rows \t' + str(n_rows) + '# number of rows of turbines (0 if no turbines)\n' + \
        'DX_f \t' + str(option['DX_f']) + '    # x position of the farm1 \n'
  for n in range(n_rows):
    turbinestring +=  'X_f' + str(n) + '\t' + str(option['X_f0'][n])  + '\t# x position of the row of turbines \n'
    turbinestring += 'Y1_f' + str(n) + '\t' + str(option['Y1_f0'][n]) + '\t# y position of the row of turbines \n'
    turbinestring += 'Y2_f' + str(n) + '\t' + str(option['Y2_f0'][n]) + '\t# y position of the row of turbines \n'
    turbinestring += 'Ct_f' + str(n) + '\t' + str(option['Ct_f0']) + '\t# Drag coef of the turbine row \n'


  print("\n Modification of suntans.dat file \n")
  fileops.replacelines('suntans.dat', 
      ['dt ', 'nsteps ', 'n_rows '],
      ['dt \t' + str(option['pdt_turb']) + '     # Time step\n',
       'nsteps \t' + str(option['nsteps_turb']) + '     # Number of steps\n',
       turbinestring])

  # Run the simulation
  print('\n Launch of the simulation r3= ', r3, '; eps= ', eps, 'n_rows= ', n_rows, '\n')
  fileops.runsuntans(option, new_dir, num_procs, 1)

  # Note auto_restart can replace the output files(e.g. velocity fields u.dat)
	# If data is needed from the full run, needs to be done here

  # Runs for convergence
  P = auto_restart(option, new_dir, option['dir_sources'], num_procs, option['dt_meanP'], option['marge'], option['pdt_restart'], option['nsteps_restart'])

  #################SAVE POWER IN A FILE###########################
  os.chdir(dir_all + '/sim_' + str(option['run_number']))
  File = open('all_powers', 'a')
  File.write(str(r3) + '\t' + str(eps) + '\t' + str(option['r1']) + '\t' + str(P * option['r1'] / (3600 * 12)) + '\n')
  File.close()

  powertotal = P * option['r1'] / (3600 * 12)
  return powertotal

